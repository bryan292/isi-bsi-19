function addClass(el, className) {
    if (el.classList)
        el.classList.add(className);
    else
        el.className += ' ' + className;
};
function removeClass(el, className) {
    if (el.classList)
        el.classList.remove(className);
    else
        el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
};
function isHidden(el) {
    var style = window.getComputedStyle(el);
    return ((style.display === 'none') || (style.visibility === 'hidden'))
};

function onFocusSearchBox() {
    addClass(document.getElementById("search-bar-id"), "search-bar");
    document.getElementById("mySearch").style.visibility = "visible";
    removeClass(document.getElementById("search-bar-id"), 'search-bar-colapsed');
};

function onBlurSearchBox() {
    filter = document.getElementById("mySearch").value;
    if (filter.length > 0) {

    } else {
        addClass(document.getElementById("search-bar-id"), "search-bar-colapsed");
        document.getElementById("mySearch").style.visibility = "hidden";
        document.getElementById("mySearch").style.display = 'none';
        removeClass(document.getElementById("search-bar-id"), 'search-bar');
    }

};

function onclickSearchIcon() {
    addClass(document.getElementById("search-bar-id"), "search-bar");
    removeClass(document.getElementById("search-bar-id"), 'search-bar-colapsed');
    document.getElementById("mySearch").style.display = 'inline';
    document.getElementById("mySearch").style.visibility = "visible";
    document.getElementById("mySearch").focus();
};
function myFunction(input) {
    // Declare variables
    var filter, ul, li, a, i, panels, containers, container_links;
    panels = document.getElementsByClassName("panel-collapse");
    containers = document.getElementsByClassName("container");
    filter = document.getElementById(input).value.toUpperCase();
    if (filter.length > 0) {
        for (x = 0; x < panels.length; x++) {
            addClass(document.getElementById(panels[x].id), "in");
        }

    }
    else {
        for (x = 0; x < panels.length; x++) {
            removeClass(document.getElementById(panels[x].id), 'in');
        }
    }
    ul = document.getElementById("myMenu");
    li = document.getElementsByTagName("li");

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";

        } else {
            //make the list item disapear
            li[i].style.display = "none";
        }
        for (x = 1; x < containers.length; x++) {
            container_links = containers[x].getElementsByTagName("li");
            var visible = true;
            for (let y = 0; y < container_links.length; y++) {
                if (isHidden(container_links[y])) {
                    // hidden
                    visible = false;
                }
                else {
                    // visible
                    visible = true;
                    break;
                }
            }
            if (!visible) {
                document.getElementById(containers[x].id).style.display = "none";
            } else {
                document.getElementById(containers[x].id).style.display = "";
            }
        }
    }
}
