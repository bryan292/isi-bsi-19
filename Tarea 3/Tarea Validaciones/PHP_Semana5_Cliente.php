<?php 
$name = strtolower($_POST["name"]); 
$lastName = strtolower($_POST["lastName"]); 
$password1 = $_POST["password1"]; 
$password2 = $_POST["password2"]; 
$email = $_POST["email"]; 
$birthDate = $_POST["birthDate"]; 
$hiddenField = $_POST["hiddenField"];
$errorMessage = "";
$successMessage = "";
if($hiddenField == "")
{
	if($name == "")
	{
		$errorMessage = $errorMessage."Server Message: El campo de Nombre de Usuario es obligatorio<br>";		
	}
	if($lastName == "")
	{
		$errorMessage = $errorMessage."Server Message: El campo de Apellido de Usuario es obligatorio<br>";
	}
	if($password1 == "")
	{
		$errorMessage = $errorMessage."Server Message: El campo de Contraseña es obligatorio<br>";
	}
	if($password2 == "")
	{
		$errorMessage = $errorMessage."Server Message: El campo de Confirmacion de Contraseña es obligatorio<br>";
	}
	if($email == "")
	{
		$errorMessage = $errorMessage."Server Message: El campo de Correo Electronico es obligatorio<br>";
	}
	if($birthDate == "")
	{
		$errorMessage = $errorMessage."Server Message: El campo de Fecha de Nacimiento es obligatorio<br>";
	}	
	
    if (!preg_match('([A-Z]{1,})', $password1))
    {
        $errorMessage = $errorMessage."Server Message: La contraseña debe tener al menos una Mayuscula<br>";
    }
    if (!preg_match('([a-z]{1,})', $password1))
    {
        $errorMessage = $errorMessage."Server Message: La contraseña debe tener al menos una Minuscula<br>";
    }
    if (!preg_match('([0-9]{1,})', $password1))
    {
        $errorMessage = $errorMessage."Server Message: La contraseña debe tener al menos un Numero<br>";
    }
    if (!preg_match('([-!$%^&*()_+|~=`{}\[\]:";\'<>?,.\/]{1,})', $password1))
    {
        $errorMessage = $errorMessage."Server Message: La contraseña debe tener al menos un Simbolo<br>";
    }
    
    if ($password1 != $password2) {
        $errorMessage = $errorMessage."Server Message: Las contraseñas deben ser iguales<br>";
    }
    
    if (!preg_match('(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)', $email))
    {
        $errorMessage = $errorMessage."Server Message: El formato de correo es incorrecto<br>";
    }

}else
{
    $errorMessage = $errorMessage."Server Message: Hubo un intento de vulnerar el formulario<br>";
}
if ($errorMessage == "") {
    $successMessage = $successMessage."Informacion Correcta<br>";
    $successMessage = $successMessage."Nombre de Usuario = ".$name.'.'.$lastName."<br>";
    $successMessage = $successMessage."Correo Electronico = ".$email."<br>";
    $successMessage = $successMessage."Fecha de nacimiento = ".$birthDate."<br>";
    echo $successMessage;
}else {
    echo $errorMessage;
}


?>
