-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 26, 2019 at 05:04 AM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_libros`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `aumentar10_libro` (IN `editorial_libro_par` VARCHAR(20))  NO SQL
UPDATE libro
set precio_libro = precio_libro * 1.10
where editorial_libro = editorial_libro_par$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `aumentar_value_by_editorial` (IN `editorial_libro_par` VARCHAR(20), IN `precio_libro_par` DECIMAL(5.2))  NO SQL
UPDATE libro
set precio_libro = precio_libro + (precio_libro*(precio_libro_par*0.01))
where editorial_libro = editorial_libro_par$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_libro` (IN `id_libro_par` INT)  NO SQL
DELETE
FROM libro
WHERE id_libro = id_libro_par$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_libro` (IN `titulo_libro_par` VARCHAR(40), IN `autor_libro_par` VARCHAR(30), IN `editorial_libro_par` VARCHAR(20), IN `precio_libro_par` DECIMAL(5.2))  NO SQL
INSERT INTO `libro` (`titulo_libro`, `autor_libro`, `editorial_libro`, `precio_libro`) 

VALUES (titulo_libro_par, autor_libro_par, editorial_libro_par, precio_libro_par)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `list_all_editorial` ()  NO SQL
SELECT DISTINCT(editorial_libro)
FROM libro$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `list_all_libros` ()  NO SQL
SELECT *
FROM libro$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `list_libro_by_id` (IN `id_libro_par` INT)  NO SQL
SELECT *
FROM libro
WHERE id_libro = id_libro_par$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_libro` (IN `id_libro_par` INT, IN `titulo_libro_par` VARCHAR(40), IN `autor_libro_par` VARCHAR(30), IN `editorial_libro_par` VARCHAR(20), IN `precio_libro_par` DECIMAL(5.2))  NO SQL
UPDATE `libro` 
SET `titulo_libro` = titulo_libro_par, 
`autor_libro` = autor_libro_par, 
`editorial_libro` = editorial_libro_par, 
`precio_libro` = precio_libro_par 
WHERE `libro`.`id_libro` = id_libro_par$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `libro`
--

CREATE TABLE `libro` (
  `id_libro` int(11) NOT NULL,
  `titulo_libro` varchar(40) NOT NULL,
  `autor_libro` varchar(30) NOT NULL,
  `editorial_libro` varchar(20) NOT NULL,
  `precio_libro` decimal(5,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `libro`
--

INSERT INTO `libro` (`id_libro`, `titulo_libro`, `autor_libro`, `editorial_libro`, `precio_libro`) VALUES
(3, 'updasd', 'dadae', '1', '110'),
(4, 'wrwr', 'erwrwre', '1', '110'),
(5, '342424', '4456', '2', '123'),
(6, '345', '4456', '2', '123'),
(7, '345', '4456', '2', '123'),
(8, '345', '4456', '3', '620'),
(9, '345', '4456', '3', '620');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `libro`
--
ALTER TABLE `libro`
  ADD PRIMARY KEY (`id_libro`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `libro`
--
ALTER TABLE `libro`
  MODIFY `id_libro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
