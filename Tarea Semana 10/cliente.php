<?php 
include_once($_SERVER['DOCUMENT_ROOT']."/db_manager.php");

$libro_JSON = $_POST["libro_JSON"];

$libro = json_decode($libro_JSON,true);
//echo $libro["titulo"];
if (isset($_POST['libro_JSON']) )
{
    if (array_key_exists("hacer",$libro))
    {
        $hacer = $libro["hacer"];
    }
    if (array_key_exists("id_libro",$libro))
    {
        $id_libro = $libro["id_libro"];
    }
    if (array_key_exists("titulo_libro",$libro))
    {
        $titulo_libro = $libro["titulo_libro"];
    }
    if (array_key_exists("autor_libro",$libro))
    {
        $autor_libro = $libro["autor_libro"];
    }
    if (array_key_exists("editorial_libro",$libro))
    {
        $editorial_libro = $libro["editorial_libro"];
    }
    if (array_key_exists("precio_libro",$libro))
    {
        $precio_libro = $libro["precio_libro"];
    }
    if (array_key_exists("porcentaje",$libro))
    {
        $porcentaje = $libro["porcentaje"];
    }
    
}

//echo $libro_JSON;
//echo $libro["hacer"];

if ($hacer =="I") {
    insert_libro($titulo_libro,$autor_libro,$editorial_libro,$precio_libro);
}
if ($hacer =="L") {
    list_all_libros();
}

if ($hacer =="LID") {
    list_libro_by_id($id_libro);
}

if ($hacer =="D") {
    delete_libro($id_libro);
}

if ($hacer =="U") {
    update_libro($id_libro,$titulo_libro,$autor_libro,$editorial_libro,$precio_libro);
}

if ($hacer =="LE") {
    list_all_editorial();
}

if ($hacer =="10") {
    aumentar10_libro($editorial_libro);
}

if ($hacer =="P") {
    aumentar_value_by_editorial($editorial_libro,$porcentaje);
}


function insert_libro($titulo_libro,$autor_libro,$editorial_libro,$precio_libro){
    $data_base = new data_base; //instancia de clase
    $data_base->set_params(array($titulo_libro,$autor_libro,$editorial_libro,$precio_libro));
    $data_base->set_store_procedure("insert_libro");
    $result = $data_base ->run_store_procedure();
    echo json_encode($result);
}

function update_libro($id_libro,$titulo_libro,$autor_libro,$editorial_libro,$precio_libro){
    $data_base = new data_base; //instancia de clase
    $data_base->set_params(array($id_libro,$titulo_libro,$autor_libro,$editorial_libro,$precio_libro));
    $data_base->set_store_procedure("update_libro");
    $result = $data_base ->run_store_procedure();
    echo json_encode($result);
}

function list_all_libros(){
    $data_base = new data_base; //instancia de clase
    $data_base->set_store_procedure("list_all_libros");
    $result = $data_base ->run_store_procedure();
    echo json_encode($result);
    //exit;
}

function list_all_editorial(){
    $data_base = new data_base; //instancia de clase
    $data_base->set_store_procedure("list_all_editorial");
    $result = $data_base ->run_store_procedure();
    echo json_encode($result);
    //exit;
}

function list_libro_by_id($id_libro){
    $data_base = new data_base; //instancia de clase
    $data_base->set_params(array($id_libro));
    $data_base->set_store_procedure("list_libro_by_id");
    $result = $data_base ->run_store_procedure();
    echo json_encode($result);
    //exit;
}

function aumentar10_libro($editorial_libro){
    $data_base = new data_base; //instancia de clase
    $data_base->set_params(array($editorial_libro));
    $data_base->set_store_procedure("aumentar10_libro");
    $result = $data_base ->run_store_procedure();
    echo json_encode($result);
    //exit;
}

function aumentar_value_by_editorial($editorial_libro,$porcentaje){
    $data_base = new data_base; //instancia de clase
    $data_base->set_params(array($editorial_libro,$porcentaje));
    $data_base->set_store_procedure("aumentar_value_by_editorial");
    $result = $data_base ->run_store_procedure();
    echo json_encode($result);
    //exit;
}

function delete_libro($id_libro){
    $data_base = new data_base; //instancia de clase
    $data_base->set_params(array($id_libro));
    $data_base->set_store_procedure("delete_libro");
    $result = $data_base ->run_store_procedure();
    echo json_encode($result);
    //exit;
}

$errorMessage = "";
$successMessage = "";

?>