<?php 

$cedula = $_POST["cedula"];
$nombre = $_POST["nombre"];
$apellido1 = $_POST["apellido1"];
$apellido2 = $_POST["apellido2"];
$telefono = $_POST["telefono"];
$provincia = $_POST["provincia"];
$canton = $_POST["canton"];
$distrito = $_POST["distrito"];
$direccion = $_POST["direccion"];
$errorMessage = "";
$successMessage = "";

if($cedula == "")
{
    $errorMessage = $errorMessage."Server Message: El campo de Cedula es obligatorio<br>";		
}
if(strlen($cedula) <> 9)
{
    $errorMessage = $errorMessage."Server Message: El campo de Cedula debe contener 9 digitos y actualmente contiene: ".strlen($cedula)."<br>";
}
if (!preg_match('(^[0-9]+$)', $cedula))
{
    $errorMessage = $errorMessage."Server Message: El campo de Cedula solamente debe contener numeros<br>";
}
if($telefono == "")
{
    $errorMessage = $errorMessage."Server Message: El campo de Telefono es obligatorio<br>";
}
if(strlen($telefono) <> 8)
{
    $errorMessage = $errorMessage."Server Message: El campo de Telefono debe contener 8 digitos y actualmente contiene: ".strlen($cedula)."<br>";
}
if (!preg_match('(^[0-9]+$)', $telefono))
{
    $errorMessage = $errorMessage."Server Message: El campo de Telefono solamente debe contener numeros<br>";
}
if($direccion == "")
{
    $errorMessage = $errorMessage."Server Message: El campo de Direccion es obligatorio<br>";
}

if ($errorMessage == "") {
    $successMessage = $successMessage."Informacion Correcta<br>";
    echo $successMessage;
}else {
    echo $errorMessage;
}
?>