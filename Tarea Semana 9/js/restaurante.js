function guardar_platillo() {
    var id_platillo_par = $("#id_platillo").val();
    var nombre_platillo_par = $("#nombre_platillo").val();
    var precio_platillo_par = $("#precio_platillo").val();
    var descripcion_platillo_par = $("#descripcion_platillo").val();
    var presentacion_platillo_par = $("#presentacion_platillo").val();
    // var num2 = $("#numero2").val();
    // var op = $("#operacion").val();

    $.ajax({
        url:"/ui/platillos/ui_platillos_guardar.php",
        type: "POST", 
        data:{
            id_platillo: id_platillo_par,
            nombre_platillo: nombre_platillo_par,
            precio_platillo: precio_platillo_par,
            descripcion_platillo: descripcion_platillo_par,
            presentacion_platillo: presentacion_platillo_par
        }, 
        success: function(result){				
            $("#output").html(result);			
        }
    });
}

function ver_platillo(id_platillo_par, action_par) {
    if (action_par == 'C') {
        $("#modal_body").show(); 
        $("#modal_message").hide(); 
        $("#btn_aceptar").hide();
        $("#btn_aceptar_cerrar").hide();
        $("#btn_guardar").hide(); 
        $("#modal_title").html("Ver Platillo");
        $("#id_platillo_output").html(id_platillo_par);
        $("#nombre_platillo_output").html($("#nombre_platillo"+id_platillo_par).html());
        $("#precio_platillo_output").html($("#precio_platillo"+id_platillo_par).html());
        $("#descripcion_platillo_output").html($("#descripcion_platillo"+id_platillo_par).html());
        $("#presentacion_platillo_output").html($("#presentacion_platillo"+id_platillo_par).html());        
    }
    if (action_par == 'D') {
        $("#modal_body").show(); 
        $("#modal_message").hide(); 
        $("#btn_aceptar_cerrar").hide(); 
        $("#btn_aceptar").show();  
        $("#btn_guardar").hide(); 
        $("#modal_title").html("Eliminar Platillo");
        $("#id_platillo_output").html(id_platillo_par);
        $("#nombre_platillo_output").html($("#nombre_platillo"+id_platillo_par).html());
        $("#precio_platillo_output").html($("#precio_platillo"+id_platillo_par).html());
        $("#descripcion_platillo_output").html($("#descripcion_platillo"+id_platillo_par).html());
        $("#presentacion_platillo_output").html($("#presentacion_platillo"+id_platillo_par).html());
    }
    if (action_par == 'U') {
        $("#modal_body").hide(); 
        $("#modal_message").show(); 
        $("#btn_guardar").show(); 
        $("#btn_cancelar").show();
        $("#btn_aceptar").hide(); 
        $("#btn_aceptar_cerrar").hide();        
        $("#modal_title").html("Actualizar Platillo");
        $("#id_platillo_output").html(id_platillo_par);
        // $("#nombre_platillo_output").html($("#nombre_platillo"+id_platillo_par).html());
        // $("#precio_platillo_output").html($("#precio_platillo"+id_platillo_par).html());
        // $("#descripcion_platillo_output").html($("#descripcion_platillo"+id_platillo_par).html());
        // $("#presentacion_platillo_output").html($("#presentacion_platillo"+id_platillo_par).html());
        $.ajax({
            url:"ui_platillos_agregar_actualizar.php",
            type: "GET", 
            data:{
                id_platillo: $("#id_platillo_output").html()
                // nombre_platillo: $("#id_platillo_output").html(),
                // precio_platillo: $("#id_platillo_output").html(),
                // descripcion_platillo: $("#id_platillo_output").html(),
                // presentacion_platillo: $("#id_platillo_output").html()
            }, 
            success: function(result){
                $("#modal_message").html(result);
            }
        });
    }

    if (action_par == 'A') {
        $("#modal_body").hide(); 
        $("#modal_message").show(); 
        $("#btn_guardar").show(); 
        $("#btn_cancelar").show();
        $("#btn_aceptar").hide(); 
        $("#btn_aceptar_cerrar").hide();        
        $("#modal_title").html("Agregar Platillo");
        $("#id_platillo_output").html(id_platillo_par);
        // $("#nombre_platillo_output").html($("#nombre_platillo"+id_platillo_par).html());
        // $("#precio_platillo_output").html($("#precio_platillo"+id_platillo_par).html());
        // $("#descripcion_platillo_output").html($("#descripcion_platillo"+id_platillo_par).html());
        // $("#presentacion_platillo_output").html($("#presentacion_platillo"+id_platillo_par).html());
        $.ajax({
            url:"ui_platillos_agregar_actualizar.php",
            type: "GET", 
            data:{
                id_platillo: $("#id_platillo_output").html(),
                // nombre_platillo: $("#id_platillo_output").html(),
                // precio_platillo: $("#id_platillo_output").html(),
                // descripcion_platillo: $("#id_platillo_output").html(),
                // presentacion_platillo: $("#id_platillo_output").html()
            }, 
            success: function(result){
                $("#modal_message").html(result);
            }
        });
    }
}

function eliminar_platillo() {
    $("#modal_body").hide(); 
    $("#modal_message").show(); 
    $("#btn_guardar").hide(); 
    $.ajax({
        url:"/logic/platillos/logic_platillos.php",
        type: "POST", 
        data:{
            id_platillo_eliminar: $("#id_platillo_output").html()
        }, 
        success: function(result){
            $("#btn_aceptar").hide(); 
            $("#btn_cancelar").hide(); 
            $("#btn_aceptar_cerrar").show(); 
            $("#modal_message").html(result);
        }
    });
}

function eliminar_platillo_refrescar() {
    $("#btn_aceptar").hide(); 
    $("#btn_cancelar").hide(); 
    $("#btn_aceptar_cerrar").show(); 
    $.ajax({
        url: "",
        context: document.body,
        success: function(s,x){
            $(this).html(s);
        }
    });
}

//-----------------------------------CLIENTE--------------------------------------------------
function guardar_cliente() {
    var id_cliente_par = $("#id_cliente").val();
    var nombre_cliente_par = $("#nombre_cliente	").val();
    var cedula_cliente_par = $("#cedula_cliente").val();
    var telefono_cliente_par = $("#telefono_cliente").val();
    var email_cliente_par = $("#email_cliente").val();
	var direccion_cliente_par = $("#direccion_cliente").val();
	var provincia_cliente_par = $("#provincia_cliente").val();
	var canton_cliente_par = $("#canton_cliente").val();
	var distrito_cliente_par = $("#distrito_cliente").val();
    // var num2 = $("#numero2").val();
    // var op = $("#operacion").val();

    $.ajax({
        url:"/ui/clientes/ui_clientes_guardar.php",
        type: "POST", 
        data:{
            id_cliente: id_cliente_par,
            nombre_cliente: nombre_cliente_par,
            cedula_cliente: cedula_cliente_par,
            telefono_cliente: telefono_cliente_par,
            email_cliente: email_cliente_par,
			direccion_cliente: direccion_cliente_par,
			provincia_cliente: provincia_cliente_par,
			canton_cliente: canton_cliente_par,
			distrito_cliente: distrito_cliente_par
        }, 
        success: function(result){				
            $("#output").html(result);
            $("#btn_aceptar").focus();
        }
    });
}

function ver_cliente(id_cliente_par, action_par) {
    if (action_par == 'C') {
        $("#modal_body").show(); 
        $("#modal_message").hide(); 
        $("#btn_aceptar").hide();
        $("#btn_aceptar_cerrar").hide();
        $("#btn_guardar").hide(); 
        $("#modal_title").html("Ver Cliente");
        $("#id_cliente_output").html(id_cliente_par);
        $("#nombre_cliente_output").html($("#nombre_cliente"+id_cliente_par).html());
        $("#cedula_cliente_output").html($("#cedula_cliente"+id_cliente_par).html());
        $("#telefono_cliente_output").html($("#telefono_cliente"+id_cliente_par).html());
        $("#email_cliente_output").html($("#email_cliente"+id_cliente_par).html());
        $("#direccion_cliente_output").html($("#direccion_cliente"+id_cliente_par).html());
        $("#provincia_cliente_output").html($("#provincia_cliente"+id_cliente_par).html());
        $("#canton_cliente_output").html($("#canton_cliente"+id_cliente_par).html());
        $("#distrito_cliente_output").html($("#distrito_cliente"+id_cliente_par).html());
    }

    if (action_par == 'D') {
        $("#modal_body").show(); 
        $("#modal_message").hide(); 
        $("#btn_aceptar_cerrar").hide(); 
        $("#btn_aceptar").show();  
        $("#btn_guardar").hide(); 
        $("#modal_title").html("Eliminar Cliente");
        $("#id_cliente_output").html(id_cliente_par);
        $("#nombre_cliente_output").html($("#nombre_cliente"+id_cliente_par).html());
        $("#cedula_cliente_output").html($("#cedula_cliente"+id_cliente_par).html());
        $("#telefono_cliente_output").html($("#telefono_cliente"+id_cliente_par).html());
        $("#email_cliente_output").html($("#email_cliente"+id_cliente_par).html());
        $("#direccion_cliente_output").html($("#direccion_cliente"+id_cliente_par).html());
        $("#provincia_cliente_output").html($("#provincia_cliente"+id_cliente_par).html());
        $("#canton_cliente_output").html($("#canton_cliente"+id_cliente_par).html());
        $("#distrito_cliente_output").html($("#distrito_cliente"+id_cliente_par).html());
    }
    if (action_par == 'U') {
        $("#modal_body").hide(); 
        $("#modal_message").show(); 
        $("#btn_guardar").show(); 
        $("#btn_cancelar").show();
        $("#btn_aceptar").hide(); 
        $("#btn_aceptar_cerrar").hide();        
        $("#modal_title").html("Actualizar Cliente");
        $("#id_cliente_output").html(id_cliente_par);
        // $("#nombre_cliente_output").html($("#nombre_cliente"+id_cliente_par).html());
        // $("#precio_cliente_output").html($("#precio_cliente"+id_cliente_par).html());
        // $("#descripcion_cliente_output").html($("#descripcion_cliente"+id_cliente_par).html());
        // $("#presentacion_cliente_output").html($("#presentacion_cliente"+id_cliente_par).html());
        $.ajax({
            url:"ui_clientes_agregar_actualizar.php",
            type: "GET", 
            data:{
                id_cliente: $("#id_cliente_output").html(),
                // nombre_cliente: $("#id_cliente_output").html(),
                // precio_cliente: $("#id_cliente_output").html(),
                // descripcion_cliente: $("#id_cliente_output").html(),
                // presentacion_cliente: $("#id_cliente_output").html()
            }, 
            success: function(result){
                $("#modal_message").html(result);
            }
        });
    }

    if (action_par == 'A') {
        $("#modal_body").hide(); 
        $("#modal_message").show(); 
        $("#btn_guardar").show(); 
        $("#btn_cancelar").show();
        $("#btn_aceptar").hide(); 
        $("#btn_aceptar_cerrar").hide();        
        $("#modal_title").html("Agregar Cliente");
        $("#id_cliente_output").html(id_cliente_par);
        // $("#nombre_cliente_output").html($("#nombre_cliente"+id_cliente_par).html());
        // $("#precio_cliente_output").html($("#precio_cliente"+id_cliente_par).html());
        // $("#descripcion_cliente_output").html($("#descripcion_cliente"+id_cliente_par).html());
        // $("#presentacion_cliente_output").html($("#presentacion_cliente"+id_cliente_par).html());
        $.ajax({
            url:"ui_clientes_agregar_actualizar.php",
            type: "GET", 
            data:{
                id_cliente: $("#id_cliente_output").html(),
                // nombre_cliente: $("#id_cliente_output").html(),
                // precio_cliente: $("#id_cliente_output").html(),
                // descripcion_cliente: $("#id_cliente_output").html(),
                // presentacion_cliente: $("#id_cliente_output").html()
            }, 
            success: function(result){
                $("#modal_message").html(result);
            }
        });
    }
}

function eliminar_cliente() {
    $("#modal_body").hide(); 
    $("#modal_message").show(); 
    $("#btn_guardar").hide(); 
    $.ajax({
        url:"/logic/clientes/logic_clientes.php",
        type: "POST", 
        data:{
            id_cliente_eliminar: $("#id_cliente_output").html()
        }, 
        success: function(result){
            $("#btn_aceptar").hide(); 
            $("#btn_cancelar").hide(); 
            $("#btn_aceptar_cerrar").show(); 
            $("#modal_message").html(result);
        }
    });
}

function eliminar_cliente_refrescar() {
    $("#btn_aceptar").hide(); 
    $("#btn_cancelar").hide(); 
    $("#btn_aceptar_cerrar").show(); 
    $.ajax({
        url: "",
        context: document.body,
        success: function(s,x){
            $(this).html(s);
        }
    });
}

//-----------------------------------ORDEN--------------------------------------------------
function guardar_orden() {
    var platillos =[];
    $('.group_platillos').each(function () {
        //alert($(this).val());
        if ($(this).hasClass("no_agregar")) {
            
        }else {
            platillos.push($(this).val());
        }        
        });
    var id_orden_par = $("#id_orden").val();
    var fecha_orden_par = $("#fecha_orden").val();
    var hora_orden_par = $("#hora_orden").val();
    var numero_mesa_orden_par = $("#numero_mesa_orden").val();
    var id_cliente_orden_par = $("#id_cliente_orden").val();
	//var id_platillo_ordenr = $("#id_platillo_orden").val();
	var estado_orden_par = $("#estado_orden").val();

    // var num2 = $("#numero2").val();
    // var op = $("#operacion").val();

    $.ajax({
        url:"/ui/ordenes/ui_ordenes_guardar.php",
        type: "POST", 
        data:{
            id_orden: id_orden_par,
            fecha_orden: fecha_orden_par,
            hora_orden: hora_orden_par,
            numero_mesa_orden: numero_mesa_orden_par,
            id_cliente_orden: id_cliente_orden_par,
			platillos_orden: platillos,
			estado_orden: estado_orden_par
        }, 
        success: function(result){				
            $("#output").html(result);
            $("#btn_aceptar").focus();
        }
    });
}

function ver_orden(id_orden_par, action_par) {
    if (action_par == 'C') {
        $("#modal_body").show(); 
        $("#modal_message").hide(); 
        $("#btn_aceptar").hide();
        $("#btn_aceptar_cerrar").hide();
        $("#btn_guardar").hide(); 
        $("#modal_title").html("Ver Orden");
        $("#id_orden_output").html(id_orden_par);
        $("#fecha_orden_output").html($("#fecha_orden"+id_orden_par).html());
        $("#hora_orden_output").html($("#hora_orden"+id_orden_par).html());
        $("#numero_mesa_orden_output").html($("#numero_mesa_orden"+id_orden_par).html());
        $("#id_cliente_orden_output").html($("#id_cliente_orden"+id_orden_par).html());
        $("#id_platillo_orden_output").html($("#id_platillo_orden"+id_orden_par).html());
        $("#estado_orden_output").html($("#estado_orden"+id_orden_par).html());
    }

    if (action_par == 'D') {
        $("#modal_body").show(); 
        $("#modal_message").hide(); 
        $("#btn_aceptar_cerrar").hide(); 
        $("#btn_aceptar").show();  
        $("#btn_guardar").hide(); 
        $("#modal_title").html("Eliminar Orden");
        $("#id_orden_output").html(id_orden_par);
        $("#fecha_orden_output").html($("#fecha_orden"+id_orden_par).html());
        $("#hora_orden_output").html($("#hora_orden"+id_orden_par).html());
        $("#numero_mesa_orden_output").html($("#numero_mesa_orden"+id_orden_par).html());
        $("#id_cliente_orden_output").html($("#id_cliente_orden"+id_orden_par).html());
        $("#id_platillo_orden_output").html($("#id_platillo_orden"+id_orden_par).html());
        $("#estado_orden_output").html($("#estado_orden"+id_orden_par).html());
    }
    if (action_par == 'U') {
        $("#modal_body").hide(); 
        $("#modal_message").show(); 
        $("#btn_guardar").show(); 
        $("#btn_cancelar").show();
        $("#btn_aceptar").hide(); 
        $("#btn_aceptar_cerrar").hide();        
        $("#modal_title").html("Actualizar Orden");
        $("#id_orden_output").html(id_orden_par);
        // $("#nombre_orden_output").html($("#nombre_orden"+id_orden_par).html());
        // $("#precio_orden_output").html($("#precio_orden"+id_orden_par).html());
        // $("#descripcion_orden_output").html($("#descripcion_orden"+id_orden_par).html());
        // $("#presentacion_orden_output").html($("#presentacion_orden"+id_orden_par).html());
        $.ajax({
            url:"ui_ordenes_agregar_actualizar.php",
            type: "GET", 
            data:{
                id_orden: $("#id_orden_output").html(),
                // nombre_orden: $("#id_orden_output").html(),
                // precio_orden: $("#id_orden_output").html(),
                // descripcion_orden: $("#id_orden_output").html(),
                // presentacion_orden: $("#id_orden_output").html()
            }, 
            success: function(result){
                $("#modal_message").html(result);
            }
        });
    }

    if (action_par == 'A') {
        $("#modal_body").hide(); 
        $("#modal_message").show(); 
        $("#btn_guardar").show(); 
        $("#btn_cancelar").show();
        $("#btn_aceptar").hide(); 
        $("#btn_aceptar_cerrar").hide();        
        $("#modal_title").html("Agregar Orden");
        $("#id_orden_output").html(id_orden_par);
        // $("#nombre_orden_output").html($("#nombre_orden"+id_orden_par).html());
        // $("#precio_orden_output").html($("#precio_orden"+id_orden_par).html());
        // $("#descripcion_orden_output").html($("#descripcion_orden"+id_orden_par).html());
        // $("#presentacion_orden_output").html($("#presentacion_orden"+id_orden_par).html());
        $.ajax({
            url:"ui_ordenes_agregar_actualizar.php",
            type: "GET", 
            data:{
                id_orden: $("#id_orden_output").html(),
                // nombre_orden: $("#id_orden_output").html(),
                // precio_orden: $("#id_orden_output").html(),
                // descripcion_orden: $("#id_orden_output").html(),
                // presentacion_orden: $("#id_orden_output").html()
            }, 
            success: function(result){
                $("#modal_message").html(result);
            }
        });
    }
}

function eliminar_orden() {
    $("#modal_body").hide(); 
    $("#modal_message").show(); 
    $("#btn_guardar").hide(); 
    $.ajax({
        url:"/logic/ordenes/logic_ordenes.php",
        type: "POST", 
        data:{
            id_orden_eliminar: $("#id_orden_output").html()
        }, 
        success: function(result){
            $("#btn_aceptar").hide(); 
            $("#btn_cancelar").hide(); 
            $("#btn_aceptar_cerrar").show(); 
            $("#modal_message").html(result);
        }
    });
}

function eliminar_orden_refrescar() {
    $("#btn_aceptar").hide(); 
    $("#btn_cancelar").hide(); 
    $("#btn_aceptar_cerrar").show(); 
    $.ajax({
        url: "",
        context: document.body,
        success: function(s,x){
            $(this).html(s);
        }
    });
}

function agregar_platillo_orden_agregar() {

    $("#platillo_cont").html(parseInt($("#platillo_cont").html()) +1);
    var nuevo_nombre_id = $("#platillo_cont").html();
    var platillo_nuevo = document.getElementById("orden_platillo_cambiar").outerHTML;
    platillo_nuevo = platillo_nuevo.replace("_cambiar",nuevo_nombre_id);
    platillo_nuevo = platillo_nuevo.replace("no_agregar","");
    $("#id_platillos").append(platillo_nuevo);
    // $("#id_platillos").append('<div class="input-group" id="orden_platillo'+ $("#platillo_cont").html() +'"> \
    //         <input  type="text" class="form-control group_platillos" placeholder="Platillo Id">\
    //         <div class="input-group-btn"> \
    //         <button onclick="eliminar_platillo_orden_agregar('+ $("#platillo_cont").html() +')"  class="btn btn-danger" type="submit"> \
    //             <i class="glyphicon glyphicon-minus"></i> \
    //         </button> \
    //         </div> \
    //     </div>');       

}



function eliminar_platillo_orden_agregar(params) {
    $("#orden_platillo" +params).remove();      

}

//------------------------------------------------------facturas---------------------------------------
function guardar_factura() {
    var id_factura_par = $("#id_factura").val();
    var fecha_factura_par = $("#fecha_factura	").val();
    var hora_factura_par = $("#hora_factura").val();
    var id_orden_factura_par = $("#id_orden_factura").val();
    var subtotal_factura_par = $("#subtotal_factura").val();
	var impuesto_servicio_factura_par = $("#impuesto_servicio_factura").val();
	var iva_factura_par = $("#iva_factura").val();
	var total_factura_par = $("#total_factura").val();
    // var num2 = $("#numero2").val();
    // var op = $("#operacion").val();

    $.ajax({
        url:"/ui/facturas/ui_facturas_guardar.php",
        type: "POST", 
        data:{
            id_factura: id_factura_par,
            fecha_factura: fecha_factura_par,
            hora_factura: hora_factura_par,
            id_orden_factura: id_orden_factura_par,
            subtotal_factura: subtotal_factura_par,
			impuesto_servicio_factura: impuesto_servicio_factura_par,
			iva_factura: iva_factura_par,
			total_factura: total_factura_par
        }, 
        success: function(result){				
            $("#output").html(result);
            $("#btn_aceptar").focus();
        }
    });
}

function ver_factura(id_factura_par, action_par) {
    if (action_par == 'C') {
        $("#modal_body").show(); 
        $("#modal_message").hide(); 
        $("#btn_aceptar").hide();
        $("#btn_aceptar_cerrar").hide();
        $("#btn_guardar").hide(); 
        $("#modal_title").html("Ver factura");
        $("#id_factura_output").html(id_factura_par);
        $("#fecha_factura_output").html($("#fecha_factura"+id_factura_par).html());
        $("#hora_factura_output").html($("#hora_factura"+id_factura_par).html());
        $("#id_orden_factura_output").html($("#id_orden_factura"+id_factura_par).html());
        $("#subtotal_factura_output").html($("#subtotal_factura"+id_factura_par).html());
        $("#impuesto_servicio_factura_output").html($("#impuesto_servicio_factura"+id_factura_par).html());
        $("#iva_factura_output").html($("#iva_factura"+id_factura_par).html());
        $("#total_facturaoutput").html($("#total_factura"+id_factura_par).html());
    }

    if (action_par == 'D') {
        $("#modal_body").show(); 
        $("#modal_message").hide(); 
        $("#btn_aceptar_cerrar").hide(); 
        $("#btn_aceptar").show();  
        $("#btn_guardar").hide(); 
        $("#modal_title").html("Eliminar factura");
        $("#id_factura_output").html(id_factura_par);
        $("#fecha_factura_output").html($("#fecha_factura"+id_factura_par).html());
        $("#hora_factura_output").html($("#hora_factura"+id_factura_par).html());
        $("#id_orden_factura_output").html($("#id_orden_factura"+id_factura_par).html());
        $("#subtotal_factura_output").html($("#subtotal_factura"+id_factura_par).html());
        $("#impuesto_servicio_factura_output").html($("#impuesto_servicio_factura"+id_factura_par).html());
        $("#iva_factura_output").html($("#iva_factura"+id_factura_par).html());
        $("#total_factura_output").html($("#total_factura"+id_factura_par).html());
    }
    if (action_par == 'U') {
        $("#modal_body").hide(); 
        $("#modal_message").show(); 
        $("#btn_guardar").show(); 
        $("#btn_cancelar").show();
        $("#btn_aceptar").hide(); 
        $("#btn_aceptar_cerrar").hide();        
        $("#modal_title").html("Actualizar factura");
        $("#id_factura_output").html(id_factura_par);
        // $("#nombre_factura_output").html($("#nombre_factura"+id_factura_par).html());
        // $("#precio_factura_output").html($("#precio_factura"+id_factura_par).html());
        // $("#descripcion_factura_output").html($("#descripcion_factura"+id_factura_par).html());
        // $("#presentacion_factura_output").html($("#presentacion_factura"+id_factura_par).html());
        $.ajax({
            url:"ui_facturas_agregar_actualizar.php",
            type: "GET", 
            data:{
                id_factura: id_factura_par,
                // nombre_factura: $("#id_factura_output").html(),
                // precio_factura: $("#id_factura_output").html(),
                // descripcion_factura: $("#id_factura_output").html(),
                // presentacion_factura: $("#id_factura_output").html()
            }, 
            success: function(result){
                $("#modal_message").html(result);
            }
        });
    }

    if (action_par == 'A') {
        $("#modal_body").hide(); 
        $("#modal_message").show(); 
        $("#btn_guardar").show(); 
        $("#btn_cancelar").show();
        $("#btn_aceptar").hide(); 
        $("#btn_aceptar_cerrar").hide();        
        $("#modal_title").html("Agregar factura");
        $("#id_factura_output").html(id_factura_par);
        // $("#nombre_factura_output").html($("#nombre_factura"+id_factura_par).html());
        // $("#precio_factura_output").html($("#precio_factura"+id_factura_par).html());
        // $("#descripcion_factura_output").html($("#descripcion_factura"+id_factura_par).html());
        // $("#presentacion_factura_output").html($("#presentacion_factura"+id_factura_par).html());
        $.ajax({
            url:"ui_facturas_agregar_actualizar.php",
            type: "GET", 
            data:{
                id_factura: $("#id_factura_output").html(),
                // nombre_factura: $("#id_factura_output").html(),
                // precio_factura: $("#id_factura_output").html(),
                // descripcion_factura: $("#id_factura_output").html(),
                // presentacion_factura: $("#id_factura_output").html()
            }, 
            success: function(result){
                $("#modal_message").html(result);
            }
        });
    }
}

function eliminar_factura() {
    $("#modal_body").hide(); 
    $("#modal_message").show(); 
    $("#btn_guardar").hide(); 
    $.ajax({
        url:"/logic/facturas/logic_facturas.php",
        type: "POST", 
        data:{
            id_factura_eliminar: $("#id_factura_output").html()
        }, 
        success: function(result){
            $("#btn_aceptar").hide(); 
            $("#btn_cancelar").hide(); 
            $("#btn_aceptar_cerrar").show(); 
            $("#modal_message").html(result);
        }
    });
}

function eliminar_factura_refrescar() {
    $("#btn_aceptar").hide(); 
    $("#btn_cancelar").hide(); 
    $("#btn_aceptar_cerrar").show(); 
    $.ajax({
        url: "",
        context: document.body,
        success: function(s,x){
            $(this).html(s);
        }
    });
}
//-----------------------------------------------------menu------------------------------------------
$(document).ready(function() {

	$('.burger').click(function(){
		$('header').toggleClass('clicked');
	});

	$('nav ul li').click(function(){
		$('nav ul li').removeClass('selected');
		$('nav ul li').addClass('notselected');
		$(this).toggleClass('selected');
		$(this).removeClass('notselected');
	});
	
});