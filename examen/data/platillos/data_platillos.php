<?php
include_once($_SERVER['DOCUMENT_ROOT']."/data/db_manager.php");
class data_platillos
{    
    public function list_all_platillos_index(){
        $data_base = new data_base; //instancia de clase
        $data_base->set_store_procedure("list_all_platillos_index");
        $result = $data_base ->run_store_procedure();
        return $result;
    }

    public function insert_platillo($nombre_platillo_par,$precio_platillo_par,$descripcion_platillo_par,$presentacion_platillo_par){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($nombre_platillo_par,$precio_platillo_par,$descripcion_platillo_par,$presentacion_platillo_par));
        $data_base->set_store_procedure("insert_platillo");
        $result = $data_base ->run_store_procedure_insert_update();
        return $data_base->get_result();
    }

    public function update_platillo($id_platillo_par,$nombre_platillo_par,$precio_platillo_par,$descripcion_platillo_par,$presentacion_platillo_par){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($id_platillo_par,$nombre_platillo_par,$precio_platillo_par,$descripcion_platillo_par,$presentacion_platillo_par));
        $data_base->set_store_procedure("update_platillo");
        $data_base ->run_store_procedure_insert_update();
        return $data_base->get_result();
    }

    public function delete_platillo($id_platillo_par){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($id_platillo_par));
        $data_base->set_store_procedure("delete_platillo");
        $data_base ->run_store_procedure_insert_update();
        // echo $data_base->get_result();
        // exit;
        return $data_base->get_result();
    }

    public function list_platillo_by_id($id_platillo_par){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($id_platillo_par));
        $data_base->set_store_procedure("list_platillo_by_id");
        $result = $data_base ->run_store_procedure();
        return $result;
    }
}


