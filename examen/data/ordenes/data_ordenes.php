<?php
include_once($_SERVER['DOCUMENT_ROOT']."/data/db_manager.php");
class data_ordenes
{    
    public function list_all_ordenes(){
        $data_base = new data_base; //instancia de clase
        $data_base->set_store_procedure("list_all_ordenes");
        $result = $data_base ->run_store_procedure();
        return $result;
    }

    public function insert_orden($fecha_orden_par,$hora_orden_par,$numero_mesa_orden_par,$id_cliente_orden_par,$estado_orden_par){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($fecha_orden_par,$hora_orden_par,$numero_mesa_orden_par,$id_cliente_orden_par,$estado_orden_par));
        $data_base->set_store_procedure("insert_orden");
        $result = $data_base ->run_store_procedure_insert_update();
        session_start();
        $_SESSION["message"] = $data_base ->get_result();
        return $data_base ->get_last_id();
        // exit;
        
        //return $data_base->get_result();
    }

    public function insert_platillo_orden($id_orden_par,$id_platillo_par){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($id_orden_par,$id_platillo_par));
        $data_base->set_store_procedure("insert_platillo_orden");
        $result = $data_base ->run_store_procedure_insert_update();
        //session_start();
        $_SESSION["message"] = $data_base ->get_result();
        return $data_base ->get_last_id();
        // exit;
        //return $data_base->get_result();
    }
    

    public function update_orden($id_orden_par,$fecha_orden_par,$hora_orden_par,$numero_mesa_orden_par,$id_cliente_orden_par,$estado_orden_par){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($id_orden_par,$fecha_orden_par,$hora_orden_par,$numero_mesa_orden_par,$id_cliente_orden_par,$estado_orden_par));
        $data_base->set_store_procedure("update_orden");
        $data_base ->run_store_procedure_insert_update();
        //session_start();
        $_SESSION["message"] = $data_base ->get_result();
        return $data_base ->get_last_id();
    }

    public function delete_orden($id_orden_par){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($id_orden_par));
        $data_base->set_store_procedure("delete_orden");
        $data_base ->run_store_procedure_insert_update();
        // echo $data_base->get_result();
        // exit;
        //session_start();
        $_SESSION["message"] = $data_base ->get_result();
        return $data_base->get_last_id();
    }

    public function list_orden_by_id($id_orden_par){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($id_orden_par));
        $data_base->set_store_procedure("list_orden_by_id");
        $result = $data_base ->run_store_procedure();
        return $result;
    }

    public function list_all_platillos_ordenes_by_orden($id_orden_par){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($id_orden_par));
        $data_base->set_store_procedure("list_all_platillos_ordenes_by_orden");
        $result = $data_base ->run_store_procedure();
        return $result;
    }

    public function delete_all_platillos_ordenes_by_orden($id_orden_par){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($id_orden_par));
        $data_base->set_store_procedure("delete_all_platillos_ordenes_by_orden");
        $data_base ->run_store_procedure_insert_update();
        // echo $data_base->get_result();
        // exit;
        //session_start();
        $_SESSION["message"] = $data_base ->get_result();
        return $data_base->get_last_id();
    }
}


