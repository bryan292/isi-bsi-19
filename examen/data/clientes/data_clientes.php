<?php
include_once($_SERVER['DOCUMENT_ROOT']."/data/db_manager.php");
class data_clientes
{    
    public function list_all_clientes(){
        $data_base = new data_base; //instancia de clase
        $data_base->set_store_procedure("list_all_clientes");
        $data_base ->set_params(array(''));
        $result = $data_base ->run_store_procedure();
        return $result;
    }

    public function insert_cliente($nombre_cliente_par,$cedula_cliente_par,$telefono_cliente_par,$email_cliente_par,$direccion_cliente_par,$provincia_cliente_par,$canton_cliente_par,$distrito_cliente_par){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($nombre_cliente_par,$cedula_cliente_par,$telefono_cliente_par,$email_cliente_par,$direccion_cliente_par,$provincia_cliente_par,$canton_cliente_par,$distrito_cliente_par));
        $data_base->set_store_procedure("insert_cliente");
        $result = $data_base ->run_store_procedure_insert_update();
        return $data_base->get_result();
    }

    public function update_cliente($id_cliente_par,$nombre_cliente_par,$cedula_cliente_par,$telefono_cliente_par,$email_cliente_par,$direccion_cliente_par,$provincia_cliente_par,$canton_cliente_par,$distrito_cliente_par){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($id_cliente_par,$nombre_cliente_par,$cedula_cliente_par,$telefono_cliente_par,$email_cliente_par,$direccion_cliente_par,$provincia_cliente_par,$canton_cliente_par,$distrito_cliente_par));
        $data_base->set_store_procedure("update_cliente");
        $data_base ->run_store_procedure_insert_update();
        return $data_base->get_result();
    }

    public function delete_cliente($id_cliente_par){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($id_cliente_par));
        $data_base->set_store_procedure("delete_cliente");
        $data_base ->run_store_procedure_insert_update();
        // echo $data_base->get_result();
        // exit;
        return $data_base->get_result();
    }

    public function list_cliente_by_id($id_cliente_par){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($id_cliente_par));
        $data_base->set_store_procedure("list_cliente_by_id");
        $result = $data_base ->run_store_procedure();
        return $result;
    }
}


