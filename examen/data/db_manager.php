<?php
class data_base
{
    private $con; 
    public $result = "";
    private  $columns;
    private $where;
    private $table;
    private $query;
    private $params ="";
    private $store_procedure;
    private $insertid;

      
    private function openDbConnection() 
    {
        global $con;
        $con = mysqli_connect('localhost','root','','restaurante') or die("Error al conectar ");
        //mysql_select_db('login') or die ("Error al seleccionar la Base de datos: " . mysql_error());  
        
    }
    
    private function closeDbConnection()
    {
        global $con;
        mysqli_close($con);
    }

    function set_store_procedure($store_procedure_par){
        global $store_procedure;
        $store_procedure = $store_procedure_par;
    }

    function is_inserted(){
        global $insertid;
        if ($insertid){
            return "Operacion Exitosa";
        }else{
            echo "Error en la Operacion 3";
        }
    }

    function get_last_id(){
        global $insertid;
        if ($insertid){
            //echo $insertid;
            return $insertid;
        }else{
            return "";
        }
    }

    function get_result(){
        global $result;
        if ($result){
            return "Operacion Exitosa";
        }else{
            return "Error en la Operacion 4";
        }
    }
    
    function run_store_procedure_insert_update(){
        global $params, $con, $query,$store_procedure,$insertid,$result;
        if (!empty($store_procedure)) {
            $this->openDbConnection();
            if (empty($params)) {
                $query = "CALL ".$store_procedure."()";
            }
            else{
                $query = "CALL ".$store_procedure."(".$params.")";
            }
            //echo $query;
            //exit;
            $result = mysqli_query($con,$query);// or die("Error".mysqli_error($con));
            $result2 = mysqli_query($con,"SELECT LAST_INSERT_ID()"); //or die("Error".mysqli_error($con));
            try {
                if($row = mysqli_fetch_all($result2,MYSQLI_ASSOC)){
                    $insertid = $row[0]['LAST_INSERT_ID()']; 
                    $this->closeDbConnection();                    
                    //echo $insertid;
                }else{
                    $insertid ="";
                    $this->closeDbConnection();
                }
            } catch (\Throwable $th) {
                //throw $th;
                $this->closeDbConnection();
                //return $th;
            }
        }
    }

    function run_store_procedure(){
        global $params, $con, $query,$store_procedure,$insertid,$result;
        if (!empty($store_procedure)) {
            $this->openDbConnection();
            if ($params == "''") {
                $params = "";
            }
            if (empty($params)) {
                $query = "CALL ".$store_procedure."()";
            }
            else{
                $query = "CALL ".$store_procedure."(".$params.")";
            }
            //echo $query;
            //exit;
            $result = mysqli_query($con,$query);// or die("Error".mysqli_error($con));
            if ($result) {
                try {
                    if($row = mysqli_fetch_all($result,MYSQLI_ASSOC)){
                        $insertid = $con->insert_id;
                        $this->closeDbConnection();
                        return $row; 
                    }else{
                        $this->closeDbConnection();
                        return "";
                    }
                } catch (\Throwable $th) {
                    //throw $th;
                    $this->closeDbConnection();
                    //return $th;
                }
            }
            
        }
    }

    function select()
    {        
        global $columns, $table, $where, $con, $query;
        $this->openDbConnection();
        $query = "Select ";
        //echo($query."<br>");
        //echo($columns."<br>");
        if (!empty($columns)) {
            $query = $query.$columns." From ".$table;
        }else{
            $query = $query."* From ".$table;
        }
        //echo($query."<br>");
        if (!empty($where)) {
            $query = $query." Where".$where;
        }
        //echo($query."<br>");
        //exit();
        
        $result = mysqli_query($con,$query);// or die("Error".mysqli_error($con));
        if($row = mysqli_fetch_array($result)){
            $this->closeDbConnection();
            return $row; 
        }else{
            $this->closeDbConnection();
            return ""; 
        }       
    }

    function set_params(array $params_par){
        global $params;
        if (!empty($params_par)) {
            if (count($params_par)>1) {
                $params = "'".$params_par[0]."'";
                for ($i=1; $i < count($params_par); $i++) { 
                    $params = $params.","."'".$params_par[$i]."'";
                }
            }elseif (count($params_par) == 1) {
                $params = "'".$params_par[0]."'";
                // $params = $params_par[0];
            }
        }

    }
    function set_columns(array $columns_par){
        global $columns;
        if (!empty($columns_par)) {
            if (count($columns_par)>1) {
                $columns = $columns_par[0];
                for ($i=1; $i < count($columns_par); $i++) { 
                    $columns = $columns.",".$columns_par[$i];
                }
            }elseif (count($columns_par) == 1) {
                $columns = $columns_par[0];
            }
        }
    }

    function set_where($where_par){
        global $where;
        if (!empty($where_par)) {
            if (count($where_par)>=1) {
                for ($i=0; $i < count($where_par); $i++) { 
                    $where = $where." ".$where_par[$i][0]." ".$where_par[$i][1].$where_par[$i][2]."'".$where_par[$i][3]."'";
                    //echo($where."<br>");
                }
            }
        }
        //echo($where."<br>");
        //exit();

    }

    function set_table($table_par){
        global $table;
        $table = $table_par;
    }    
}
