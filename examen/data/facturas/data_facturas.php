<?php
include_once($_SERVER['DOCUMENT_ROOT']."/data/db_manager.php");
class data_facturas
{    
    public function list_all_facturas(){
        $data_base = new data_base; //instancia de clase
        $data_base->set_store_procedure("list_all_facturas");
        $result = $data_base ->run_store_procedure();
        return $result;
    }
    public function insert_factura($fecha_factura_par,$hora_factura_par,$id_orden_factura_par,$subtotal_factura_par,$impuesto_servicio_factura,$iva_factura,$total_factura){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($fecha_factura_par,$hora_factura_par,$id_orden_factura_par,$subtotal_factura_par,$impuesto_servicio_factura,$iva_factura,$total_factura));
        $data_base->set_store_procedure("insert_factura");
        $result = $data_base ->run_store_procedure_insert_update();
        return $data_base->get_result();
    }

    public function update_factura($id_factura_par,$fecha_factura_par,$hora_factura_par,$id_orden_factura_par,$subtotal_factura_par,$impuesto_servicio_factura,$iva_factura,$total_factura){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($id_factura_par,$fecha_factura_par,$hora_factura_par,$id_orden_factura_par,$subtotal_factura_par,$impuesto_servicio_factura,$iva_factura,$total_factura));
        $data_base->set_store_procedure("update_factura");
        $data_base ->run_store_procedure_insert_update();
        return $data_base->get_result();
    }

    public function delete_factura($id_factura_par){
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($id_factura_par));
        $data_base->set_store_procedure("delete_factura");
        $data_base ->run_store_procedure_insert_update();
        // echo $data_base->get_result();
        // exit;
        return $data_base->get_result();
    }

    public function list_factura_by_id($id_factura_par){
        //  echo $id_factura_par;
        //     exit;
        $data_base = new data_base; //instancia de clase
        $data_base->set_params(array($id_factura_par));
        $data_base->set_store_procedure("list_factura_by_id");
        $result = $data_base ->run_store_procedure();
        return $result;
    }
}


