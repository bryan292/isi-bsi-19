<?php
include_once($_SERVER['DOCUMENT_ROOT']."/data/ordenes/data_ordenes.php");
if(isset($_POST['id_orden_eliminar'])){
    $id_orden = $_POST['id_orden_eliminar'];
    if (!empty($id_orden)) {
        $orden = new logic_ordenes;
        $orden -> delete_orden($id_orden);
    }
}

class logic_ordenes{    
    function list_all_ordenes()
        {
            $ordenes = new data_ordenes;
            return $ordenes ->list_all_ordenes();
        }

    function list_orden_by_id($id_orden_par)
        {
            $orden = new data_ordenes;
            return $orden ->list_orden_by_id($id_orden_par);
        }
    function insert_orden($fecha_orden_par,$hora_orden_par,$numero_mesa_orden_par,$id_cliente_orden_par,$estado_orden_par)
        {            
            $orden = new data_ordenes;
            $temp_date =date("Y-m-d", strtotime($fecha_orden_par));
            return $orden ->insert_orden($temp_date ,$hora_orden_par,$numero_mesa_orden_par,$id_cliente_orden_par,$estado_orden_par);
        }
    function insert_platillo_orden($id_orden_par,$id_platillo_par)
        {
            $orden = new data_ordenes;
            return $orden ->insert_platillo_orden($id_orden_par,$id_platillo_par);           
        }

    function update_orden($id_orden_par,$fecha_orden_par,$hora_orden_par,$numero_mesa_orden_par,$id_cliente_orden_par,$estado_orden_par)
        {
            $temp_date =date("Y-m-d", strtotime($fecha_orden_par));
            $orden = new data_ordenes;
            return $orden ->update_orden($id_orden_par,$temp_date,$hora_orden_par,$numero_mesa_orden_par,$id_cliente_orden_par,$estado_orden_par);
        }

    function delete_orden($id_orden_par)
        {
            $orden = new data_ordenes;
            $orden ->delete_orden($id_orden_par);
            //echo $_SESSION["message"];
        }
    function list_all_platillos_ordenes_by_orden($id_orden_par)
        {
            $orden = new data_ordenes;
            return $orden ->list_all_platillos_ordenes_by_orden($id_orden_par);
        }

    function delete_all_platillos_ordenes_by_orden($id_orden_par)
        {
            $orden = new data_ordenes;
            $orden ->delete_all_platillos_ordenes_by_orden($id_orden_par);
            //echo $_SESSION["message"];
        }
}