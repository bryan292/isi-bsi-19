<?php
include_once($_SERVER['DOCUMENT_ROOT']."/data/facturas/data_facturas.php");
if(isset($_POST['id_factura_eliminar'])){
    $id_factura = $_POST['id_factura_eliminar'];
    if (!empty($id_factura)) {
        $factura = new logic_facturas;
        $factura -> delete_factura($id_factura);
    }
}

class logic_facturas{    
    function list_all_facturas()
        {
            $facturas = new data_facturas;
            return $facturas ->list_all_facturas();
        }

    function list_factura_by_id($id_factura_par)
        {
            // echo $id_factura_par;
            // exit;
            $factura = new data_facturas;
            return $factura ->list_factura_by_id($id_factura_par);
        }
    function insert_factura($fecha_factura_par,$hora_factura_par,$id_orden_factura_par,$subtotal_factura_par,$impuesto_servicio_factura,$iva_factura,$total_factura)
        {
            $factura = new data_facturas;
            return $factura ->insert_factura($fecha_factura_par,$hora_factura_par,$id_orden_factura_par,$subtotal_factura_par,$impuesto_servicio_factura,$iva_factura,$total_factura);
        }

    function update_factura($id_factura_par,$fecha_factura_par,$hora_factura_par,$id_orden_factura_par,$subtotal_factura_par,$impuesto_servicio_factura,$iva_factura,$total_factura)
        {
            $factura = new data_facturas;
            return $factura ->update_factura($id_factura_par,$fecha_factura_par,$hora_factura_par,$id_orden_factura_par,$subtotal_factura_par,$impuesto_servicio_factura,$iva_factura,$total_factura);
        }

    function delete_factura($id_factura_par)
        {
            $factura = new data_facturas;
            echo $factura ->delete_factura($id_factura_par);
        }
}