<?php
class logic_contenido{
    
    private $header ="";
    private $body ="";
    private $footer = "";


    public function set_header($header_par=""){
        global $header;
        $header = $header_par;

    }
    public function set_body($body_par=""){
        global $body; 
        $body = $body_par;
    }
    public function set_footer($footer_par=""){
        global $footer;
        $footer = $footer_par;
    }
    public function build_content(){
        global $header,$body,$footer;
        
        if (empty($header)) {
            $header = $_SERVER['DOCUMENT_ROOT']."/ui/header.php";
        }

        if (empty($body)) {
            $body = $_SERVER['DOCUMENT_ROOT']."/ui/body.php";

        }

        if (empty($footer)) {
            $footer = $_SERVER['DOCUMENT_ROOT']."/ui/footer.php";
        }
        if (file_exists($header)) {
            include($header);
        }else {
            echo($header);
        }

        if (file_exists($body)) {
            include($body);
        }else {
            echo($body);
        }

        if (file_exists($footer)) {
            include($footer);
        }else {
            echo($footer);
        }
    }

}



?>
