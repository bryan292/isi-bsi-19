<?php
include_once($_SERVER['DOCUMENT_ROOT']."/data/platillos/data_platillos.php");
if(isset($_POST['id_platillo_eliminar'])){
    $id_platillo = $_POST['id_platillo_eliminar'];
    if (!empty($id_platillo)) {
        $platillo = new logic_platillos;
        $platillo -> delete_platillo($id_platillo);
    }
}

class logic_platillos{    
    function list_all_platillos_index()
        {
            $platillos = new data_platillos;
            return $platillos ->list_all_platillos_index();
        }

    function list_platillo_by_id($id_platillo_par)
        {
            $platillo = new data_platillos;
            return $platillo ->list_platillo_by_id($id_platillo_par);
        }
    function insert_platillo($nombre_platillo_par,$precio_platillo_par,$descripcion_platillo_par,$presentacion_platillo_par)
        {
            $platillo = new data_platillos;
            return $platillo ->insert_platillo($nombre_platillo_par,$precio_platillo_par,$descripcion_platillo_par,$presentacion_platillo_par);
        }

    function update_platillo($id_platillo_par,$nombre_platillo_par,$precio_platillo_par,$descripcion_platillo_par,$presentacion_platillo_par)
        {
            $platillo = new data_platillos;
            return $platillo ->update_platillo($id_platillo_par,$nombre_platillo_par,$precio_platillo_par,$descripcion_platillo_par,$presentacion_platillo_par);
        }

    function delete_platillo($id_platillo_par)
        {
            $platillo = new data_platillos;
            echo $platillo ->delete_platillo($id_platillo_par);
        }
}