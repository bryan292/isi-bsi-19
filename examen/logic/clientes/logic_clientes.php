<?php
include_once($_SERVER['DOCUMENT_ROOT']."/data/clientes/data_clientes.php");
if(isset($_POST['id_cliente_eliminar'])){
    $id_cliente = $_POST['id_cliente_eliminar'];
    if (!empty($id_cliente)) {
        $cliente = new logic_clientes;
        $cliente -> delete_cliente($id_cliente);
    }
}

class logic_clientes{    
    function list_all_clientes()
        {
            $clientes = new data_clientes;
            return $clientes ->list_all_clientes();
        }

    function list_cliente_by_id($id_cliente_par)
        {
            $cliente = new data_clientes;
            return $cliente ->list_cliente_by_id($id_cliente_par);
        }
    function insert_cliente($nombre_cliente_par,$cedula_cliente_par,$telefono_cliente_par,$email_cliente_par,$direccion_cliente_par,$provincia_cliente_par,$canton_cliente_par,$distrito_cliente_par)
        {
            
            $cliente = new data_clientes;
            return $cliente ->insert_cliente($nombre_cliente_par,$cedula_cliente_par,$telefono_cliente_par,$email_cliente_par,$direccion_cliente_par,$provincia_cliente_par,$canton_cliente_par,$distrito_cliente_par);
        }

    function update_cliente($id_cliente_par,$nombre_cliente_par,$cedula_cliente_par,$telefono_cliente_par,$email_cliente_par,$direccion_cliente_par,$provincia_cliente_par,$canton_cliente_par,$distrito_cliente_par)
        {
            $cliente = new data_clientes;
            return $cliente ->update_cliente($id_cliente_par,$nombre_cliente_par,$cedula_cliente_par,$telefono_cliente_par,$email_cliente_par,$direccion_cliente_par,$provincia_cliente_par,$canton_cliente_par,$distrito_cliente_par);
        }

    function delete_cliente($id_cliente_par)
        {
            $cliente = new data_clientes;
            echo $cliente ->delete_cliente($id_cliente_par);
        }
}