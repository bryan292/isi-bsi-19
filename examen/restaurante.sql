-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2019 at 05:38 AM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restaurante`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_all_platillos_ordenes_by_orden` (IN `id_orden_par` INT)  NO SQL
DELETE
from platillo_orden
where id_orden = id_orden_par$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_cliente` (IN `id_cliente_par` INT)  NO SQL
DELETE
FROM cientes
where id_cliente = id_cliente_par$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_factura` (IN `id_factura_par` INT)  NO SQL
delete
from facturas
where id_factura = id_factura_par$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_orden` (IN `id_orden_par` INT)  NO SQL
DELETE
from ordenes
WHERE id_orden = id_orden_par$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_platillo` (IN `id_platillo_par` INT)  NO SQL
delete 
From platillos
where id_platillo = id_platillo_par$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_platillo_orden` (IN `id_platillo_orden_par` INT)  NO SQL
DELETE
FROM platillo_orden
WHERE id_platillo_orden = id_platillo_orden_par$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_cliente` (IN `nombre_cliente_par` VARCHAR(100), IN `cedula_cliente_par` INT, IN `telefono_cliente_par` VARCHAR(15), IN `email_cliente_par` VARCHAR(100), IN `direccion_cliente_par` VARCHAR(200), IN `provincia_cliente_par` VARCHAR(100), IN `canton_cliente_par` VARCHAR(100), IN `distrito_cliente_par` VARCHAR(100))  NO SQL
INSERT INTO `clientes` (`nombre_cliente`, `cedula_cliente`, `telefono_cliente`, `email_cliente`, `direccion_cliente`, `provincia_cliente`, `canton_cliente`, `distrito_cliente`) VALUES (nombre_cliente_par, cedula_cliente_par, telefono_cliente_par, email_cliente_par, direccion_cliente_par, provincia_cliente_par, canton_cliente_par, distrito_cliente_par)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_factura` (IN `fecha_factura_par` DATE, IN `hora_factura_par` TIME, IN `id_orden_factura_par` INT, IN `subtotal_factura_par` DECIMAL(9,2), IN `impuesto_servicio_factura_par` DECIMAL(9,2), IN `iva_factura_par` DECIMAL(9,2), IN `total_factura_par` DECIMAL(9,2))  NO SQL
INSERT INTO `facturas` (
                        `fecha_factura`, 
                        `hora_factura`, 
                        `id_orden_factura`, 
                        `subtotal_factura`, 
                        `impuesto_servicio_factura`, 
                        `iva_factura`, 
                        `total_factura`)
	VALUES (fecha_factura_par, 
            hora_factura_par, 
            id_orden_factura_par, 
            subtotal_factura_par, 
            impuesto_servicio_factura_par, 
            iva_factura_par, 
            total_factura_par)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_orden` (IN `fecha_orden_par` DATE, IN `hora_orden_par` TIME, IN `numero_mesa_orden_par` TINYINT, IN `id_cliente_orden_par` INT, IN `estado_orden_par` CHAR(1))  NO SQL
INSERT INTO `ordenes` (`fecha_orden`, 
                       `hora_orden`, 
                       `numero_mesa_orden`, 
                       `id_cliente_orden`, 
                       `estado_orden`) 
VALUES (fecha_orden_par, 
        hora_orden_par, 
        numero_mesa_orden_par, 
        id_cliente_orden_par, 
        estado_orden_par)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_platillo` (IN `nombre_platillo_par` VARCHAR(50), IN `precio_platillo_par` DECIMAL(7,2), IN `presentacion_platillo_par` VARCHAR(50), IN `descripcion_platillo_par` VARCHAR(200))  NO SQL
INSERT INTO `platillos` (`id_platillo`, `nombre_platillo`, `precio_platillo`, `descripcion_platillo`, `presentacion_platillo`) VALUES (NULL, nombre_platillo_par, precio_platillo_par, descripcion_platillo_par, presentacion_platillo_par)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_platillo_orden` (IN `id_orden_par` INT, IN `id_platillo_par` INT)  NO SQL
INSERT INTO `platillo_orden` (`id_orden`,`id_platillo`) 
VALUES (id_orden_par,id_platillo_par)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `list_all_clientes` ()  NO SQL
SELECT *
FROM clientes$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `list_all_facturas` ()  NO SQL
SELECT *
FROM facturas$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `list_all_ordenes` ()  NO SQL
select *
From ordenes$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `list_all_platillos_index` ()  NO SQL
SELECT *
FROM platillos$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `list_all_platillos_ordenes_by_orden` (IN `id_orden_par` INT)  NO SQL
SELECT a.id_platillo_orden, a.id_platillo, a.id_orden, b.nombre_platillo
FROM platillo_orden a, platillos b
WHERE a.id_orden = id_orden_par
and a.id_platillo = b.id_platillo$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `list_cliente_by_id` (IN `id_cliente_par` INT)  NO SQL
SELECT *
FROM clientes
WHERE id_cliente = id_cliente_par$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `list_factura_by_id` (IN `id_factura_par` INT)  NO SQL
SELECT *
from facturas
where id_factura = id_factura_par$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `list_orden_by_id` (IN `id_orden_par` INT)  NO SQL
SELECT *
FROM ordenes
WHERE id_orden = id_orden_par$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `list_platillo_by_id` (IN `id_platillo_par` INT)  NO SQL
SELECT *
FROM platillos
WHERE id_platillo = id_platillo_par$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_cliente` (IN `id_cliente_par` INT, IN `nombre_cliente_par` VARCHAR(100), IN `cedula_cliente_par` INT, IN `telefono_cliente_par` VARCHAR(15), IN `email_cliente_par` VARCHAR(100), IN `direccion_cliente_par` VARCHAR(200), IN `provincia_cliente_par` VARCHAR(100), IN `canton_cliente_par` VARCHAR(100), IN `distrito_cliente_par` VARCHAR(100))  NO SQL
UPDATE `clientes` 
SET `nombre_cliente` = nombre_cliente_par, 
`cedula_cliente` = cedula_cliente_par, 
`telefono_cliente` = telefono_cliente_par, 
`email_cliente` = email_cliente_par, 
`direccion_cliente` = direccion_cliente_par, 
`provincia_cliente` = provincia_cliente_par, 
`canton_cliente` = canton_cliente_par, 
`distrito_cliente` = distrito_cliente_par 
WHERE `clientes`.`id_cliente` = id_cliente_par$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_factura` (IN `id_factura_par` INT, IN `fecha_factura_par` INT, IN `hora_factura_par` INT, IN `id_orden_factura_par` INT, IN `subtotal_factura_par` INT, IN `impuesto_servicio_factura_par` INT, IN `iva_factura_par` INT, IN `total_factura_par` INT)  NO SQL
UPDATE `facturas` 
SET `fecha_factura` = fecha_factura_par, 
`hora_factura` = hora_factura_par, 
`id_orden_factura` = id_orden_factura_par, 
`subtotal_factura` = subtotal_factura_par, 
`impuesto_servicio_factura` = impuesto_servicio_factura_par, 
`iva_factura` = iva_factura_par, 
`total_factura` = total_factura_par
WHERE 
`facturas`.`id_factura` = id_factura_par$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_orden` (IN `id_orden_par` INT, IN `fecha_orden_par` DATE, IN `hora_orden_par` TIME, IN `numero_mesa_orden_par` TINYINT, IN `id_cliente_orden_par` INT, IN `estado_orden_par` CHAR(1))  NO SQL
UPDATE `ordenes` 
SET
fecha_orden = fecha_orden_par, 
hora_orden = hora_orden_par,
numero_mesa_orden = numero_mesa_orden_par, 
id_cliente_orden = id_cliente_orden_par,
estado_orden = estado_orden_par

WHERE ordenes.id_orden = id_orden_par$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_platillo` (IN `id_platillo_par` INT, IN `nombre_platillo_par` VARCHAR(50), IN `precio_platillo_par` DECIMAL(7,2), IN `descripcion_platillo_par` VARCHAR(200), IN `presentacion_platillo_par` VARCHAR(50))  NO SQL
update `platillos`
set nombre_platillo = nombre_platillo_par,
precio_platillo = precio_platillo_par,
descripcion_platillo = descripcion_platillo_par,
presentacion_platillo = presentacion_platillo_par
where id_platillo = id_platillo_par$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `id_cliente` int(11) NOT NULL,
  `nombre_cliente` varchar(100) NOT NULL,
  `cedula_cliente` int(11) NOT NULL,
  `telefono_cliente` varchar(15) DEFAULT NULL,
  `email_cliente` varchar(100) NOT NULL,
  `direccion_cliente` varchar(200) DEFAULT NULL,
  `provincia_cliente` varchar(100) NOT NULL,
  `canton_cliente` varchar(100) NOT NULL,
  `distrito_cliente` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `nombre_cliente`, `cedula_cliente`, `telefono_cliente`, `email_cliente`, `direccion_cliente`, `provincia_cliente`, `canton_cliente`, `distrito_cliente`) VALUES
(7, 'nombre cliente 7', 777777, '7777', '1313', 'qqeqe1', '1', '1', '1'),
(8, 'cliente prueba', 1111, '124234', 'bryan292@outlook.com', '3401B NW 72nd Ave\nSTE 700093', 'Miami', 'prueba canton', 'prueba distr'),
(9, 'nombre cliente 9', 9999, '9999', '9999', '999', '999', '999', '999');

-- --------------------------------------------------------

--
-- Table structure for table `facturas`
--

CREATE TABLE `facturas` (
  `id_factura` int(11) NOT NULL,
  `fecha_factura` date NOT NULL,
  `hora_factura` time NOT NULL,
  `id_orden_factura` int(11) NOT NULL,
  `subtotal_factura` decimal(9,2) NOT NULL,
  `impuesto_servicio_factura` decimal(9,2) NOT NULL,
  `iva_factura` decimal(9,2) NOT NULL,
  `total_factura` decimal(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facturas`
--

INSERT INTO `facturas` (`id_factura`, `fecha_factura`, `hora_factura`, `id_orden_factura`, `subtotal_factura`, `impuesto_servicio_factura`, `iva_factura`, `total_factura`) VALUES
(1, '0000-00-00', '00:00:02', 1, '2.00', '2.00', '2.00', '2.00');

-- --------------------------------------------------------

--
-- Table structure for table `ordenes`
--

CREATE TABLE `ordenes` (
  `id_orden` int(11) NOT NULL,
  `fecha_orden` date NOT NULL,
  `hora_orden` time NOT NULL,
  `numero_mesa_orden` tinyint(4) NOT NULL,
  `id_cliente_orden` int(11) NOT NULL,
  `estado_orden` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ordenes`
--

INSERT INTO `ordenes` (`id_orden`, `fecha_orden`, `hora_orden`, `numero_mesa_orden`, `id_cliente_orden`, `estado_orden`) VALUES
(1, '2019-07-20', '07:00:20', 32, 9, 'P'),
(67, '1970-01-01', '00:00:07', 7, 7, 'P'),
(70, '1970-01-01', '00:00:07', 7, 7, 'P'),
(71, '1970-01-01', '00:00:07', 7, 7, 'L'),
(79, '2019-07-12', '12:30:00', 0, 8, 'P'),
(80, '2019-07-26', '15:02:00', 4, 9, 'P');

-- --------------------------------------------------------

--
-- Table structure for table `platillos`
--

CREATE TABLE `platillos` (
  `id_platillo` int(11) NOT NULL,
  `nombre_platillo` varchar(50) NOT NULL,
  `precio_platillo` decimal(7,2) NOT NULL,
  `descripcion_platillo` varchar(200) NOT NULL,
  `presentacion_platillo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `platillos`
--

INSERT INTO `platillos` (`id_platillo`, `nombre_platillo`, `precio_platillo`, `descripcion_platillo`, `presentacion_platillo`) VALUES
(35, 'nombre platillo 35', '35.00', 'descripcion platillo 35descripcion platillo 35descripcion platillo 35descripcion platillo 35descripcion platillo 35descripcion platillo 35descripcion platillo 35descripcion platillo 35descripcion plat', 'presentacion platillo 35'),
(36, 'nombre platillo 36', '1000.00', 'descripcion platillo 36', 'presentacion platillo 36');

-- --------------------------------------------------------

--
-- Table structure for table `platillo_orden`
--

CREATE TABLE `platillo_orden` (
  `id_platillo_orden` int(11) NOT NULL,
  `id_platillo` int(11) NOT NULL,
  `id_orden` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `platillo_orden`
--

INSERT INTO `platillo_orden` (`id_platillo_orden`, `id_platillo`, `id_orden`) VALUES
(103, 35, 1),
(104, 35, 1),
(105, 35, 1),
(106, 36, 1),
(107, 36, 1),
(108, 36, 1),
(113, 35, 80),
(114, 35, 80),
(115, 35, 80),
(116, 35, 80),
(117, 35, 80),
(126, 35, 67),
(127, 35, 67),
(128, 35, 67),
(129, 35, 67),
(130, 36, 67),
(131, 35, 70),
(132, 35, 70),
(133, 35, 70),
(137, 35, 71),
(138, 35, 71),
(139, 35, 71),
(140, 35, 71),
(142, 35, 79),
(143, 36, 79),
(144, 36, 79);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indexes for table `facturas`
--
ALTER TABLE `facturas`
  ADD PRIMARY KEY (`id_factura`),
  ADD KEY `facturas_ordenes` (`id_orden_factura`);

--
-- Indexes for table `ordenes`
--
ALTER TABLE `ordenes`
  ADD PRIMARY KEY (`id_orden`),
  ADD KEY `ordenes_clientes` (`id_cliente_orden`);

--
-- Indexes for table `platillos`
--
ALTER TABLE `platillos`
  ADD PRIMARY KEY (`id_platillo`);

--
-- Indexes for table `platillo_orden`
--
ALTER TABLE `platillo_orden`
  ADD PRIMARY KEY (`id_platillo_orden`),
  ADD KEY `platillo_orden_platillo` (`id_platillo`),
  ADD KEY `platillo_orden_oden` (`id_orden`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `facturas`
--
ALTER TABLE `facturas`
  MODIFY `id_factura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ordenes`
--
ALTER TABLE `ordenes`
  MODIFY `id_orden` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `platillos`
--
ALTER TABLE `platillos`
  MODIFY `id_platillo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `platillo_orden`
--
ALTER TABLE `platillo_orden`
  MODIFY `id_platillo_orden` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `facturas`
--
ALTER TABLE `facturas`
  ADD CONSTRAINT `facturas_ordenes` FOREIGN KEY (`id_orden_factura`) REFERENCES `ordenes` (`id_orden`);

--
-- Constraints for table `ordenes`
--
ALTER TABLE `ordenes`
  ADD CONSTRAINT `ordenes_clientes` FOREIGN KEY (`id_cliente_orden`) REFERENCES `clientes` (`id_cliente`);

--
-- Constraints for table `platillo_orden`
--
ALTER TABLE `platillo_orden`
  ADD CONSTRAINT `platillo_orden_oden` FOREIGN KEY (`id_orden`) REFERENCES `ordenes` (`id_orden`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `platillo_orden_platillo` FOREIGN KEY (`id_platillo`) REFERENCES `platillos` (`id_platillo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
