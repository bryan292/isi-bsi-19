<!DOCTYPE html>
<html>
<head>
    <title>Restaurante</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="/css/jquery-ui.css">
    <link rel="stylesheet" href="/css/datepicker.css">
    <link rel="stylesheet" href="/css/header.css">

    <script src="/js/jquery-3.4.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script> 
    <script src="/js/jquery-ui.js"></script>    
    <script src="/js/moment.js"></script> 
    <script src="/js/bootstrap-datetimepicker.min.js"></script> 
    <script src="/js/restaurante.js"></script> 

</head>
<body>
    
<header>
    <h1>Examen Restaurante</h1>          
    
	<div class="nav-modal">
		<div class="blob"></div>
		<nav>
			<ul>
				<li><a href=\ui\platillos\ui_platillos_index.php>Platillos</a>
				<li><a href=\ui\clientes\ui_clientes_index.php>Clientes</a>
				<li><a href=\ui\ordenes\ui_ordenes_index.php>Ordenes</a></li>
				<li><a href=\ui\facturas\ui_facturas_index.php>Facturas</a>
			</ul>
		</nav>
	</div>
	<div class="head">

		<div class="tile burger">
			<div class="meat">
				<div class="line one"></div>
				<div class="line two"></div>
				<div class="line three"></div>
			</div>
		</div>
	</div>
</header>


<?php
    // if (empty($_SESSION["usuario"])) {
    //     include($_SERVER['DOCUMENT_ROOT']."\ui\login\login.php");
    // }else{
    //     echo "Hola " . $_SESSION['usuario'];
    //     echo("<br>");
    //     echo('<a href=\logic\login\logic_login_logout.php>Logout</a>');
    // }
?>