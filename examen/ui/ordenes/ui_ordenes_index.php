<?php
include_once($_SERVER['DOCUMENT_ROOT']."/logic/ordenes/logic_ordenes.php");
include_once($_SERVER['DOCUMENT_ROOT']."/logic/contenido/logic_contenido.php");

//$index = new ui_list_all_ordenes_index();
//$index -> build_index();
$content = new logic_contenido;
$content -> set_header();
//$content -> set_body($_SERVER['DOCUMENT_ROOT']."/ui/ordenes/ui_list_all_ordenes_index.php");

        $body="";    
        $link ="";
        $ordenes = new logic_ordenes;
        $ordenes_array = $ordenes ->list_all_ordenes();
        $body = $body.
        '<div class="container mt-3">
        <h2>Ordenes</h2>
        <p>Desea Agregar un orden?
        <button onclick="ver_orden(\'\',\'A\')" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#checDeletekModal">Agregar</button> 
        </p>  
        <hr noshade>
        <p>Digite algun valor relacionado a la orden que desee buscar:</p>  
        <input class="form-control" id="myInput" type="text" placeholder="Buscar..">
        <br>
        <table class="table table-bordered table-condensed table-striped text-nowrap">
            <thead>
            <tr>
                <th>Identificador</th>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Numero de Mesa</th>
                <th>Cliente</th>
                <th>Platillos</th>
                <th>Estado</th>
                <th>Hacer</th>
            </tr>
            </thead>
            <tbody id="myTable" class="table-striped">';
            // echo count($ordenes_array);
            // exit;
            if (!empty($ordenes_array)) {
                
                foreach($ordenes_array as $row_key => $row)
                {
                    $platillos = "";
                    $platillos_array = $ordenes ->list_all_platillos_ordenes_by_orden($row["id_orden"]);
                    if ($platillos_array) {
                        foreach($platillos_array as $row_key_platillo => $row_platillo){
                            if (count($platillos_array)==1) {
                                $platillos = $row_platillo["nombre_platillo"];
                            }elseif (count($platillos_array)>1) {
                                if ($row_key_platillo == 0) {
                                    $platillos = $platillos.$row_platillo["nombre_platillo"];
                                }else {
                                    $platillos = $platillos.", ".$row_platillo["nombre_platillo"];
                                }
                                
                            }
                        }
                    }
                    
                    $body = $body.'<tr>
                        <td id="id_orden'.$row["id_orden"].'">'.$row["id_orden"].'</td>
                        <td id="fecha_orden'.$row["id_orden"].'">'.$row["fecha_orden"].'</td>
                        <td id="hora_orden'.$row["id_orden"].'">'.$row["hora_orden"].'</td>
                        <td id="numero_mesa_orden'.$row["id_orden"].'">'.$row["numero_mesa_orden"].'</td>
                        <td id="id_cliente_orden'.$row["id_orden"].'">'.$row["id_cliente_orden"].'</td>
                        <td id="id_platillo_orden'.$row["id_orden"].'"style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 35ch;">'.$platillos.'</td>
                        <td id="estado_orden'.$row["id_orden"].'">'.$row["estado_orden"].'</td>
                        <td>
                            <button onclick="ver_orden('.$row["id_orden"].',\'C\')" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#checDeletekModal">Ver</button> 
                            <button onclick="ver_orden('.$row["id_orden"].',\'U\')" type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#checDeletekModal">Editar</button> 
                            <button onclick="ver_orden('.$row["id_orden"].',\'D\')" type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#checDeletekModal">Eliminar</button>                           
                        </td>
                    </tr>';
                    //$body = $body.$row["nombre_orden"];// nombre de la columna
                }
            }
        $body = $body.'</tbody>
            <tfoot>
                <tr>
                    <th>Identificador</th>
                    <th>Fecha</th>
                    <th>Hora</th>
                    <th>Numero de Mesa</th>
                    <th>Cliente</th>
                    <th>Platillos</th>
                    <th>Estado</th>
                    <th>Hacer</th>
                </tr>
            </tfoot>
        </table>            
        </div>
        <!-- Modal -->
    <div id="checDeletekModal" class="modal fade" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered">    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal_title" class="modal-title">Ver orden</h4>
            </div>
            <div id="modal_body" class="modal-body">
                    <label>Identificador:</label>
                    <div>
                        <div id="id_orden_output">1</div>
                    </div>
                    <hr noshade>
                    <label>Fecha:</label>
                    <div>
                        <div id="fecha_orden_output">nombre Producto1</div>
                    </div>
                    <hr>
                    <label>Hora:</label>
                    <div>
                        <div id="hora_orden_output">111</div>
                    </div>
                    <hr>
                    <label>Numero de Mesa:</label>
                    <div>
                        <div id="numero_mesa_orden_output">111</div>
                    </div>
                    <hr>
                    <label>Cliente:</label>
                    <div>
                        <div id="id_cliente_orden_output">111</div>
                    </div>
                    <hr>
                    <label>Platillos:</label>
                    <div>
                        <div id="id_platillo_orden_output">presentacion producto 1</div>
                    </div>
                    <hr>
                    <label>Estado:</label>
                    <div>
                        <div id="estado_orden_output">presentacion producto 1</div>
                    </div>

            </div>
            <div id="modal_message" class="modal-body"></div>

            <div class="modal-footer">
            <button onclick="eliminar_orden()" id="btn_aceptar" type="button" class="btn btn-danger">Aceptar</button>
            <button onclick="eliminar_orden_refrescar()" id="btn_aceptar_cerrar" type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
            <button id="btn_guardar" onclick="guardar_orden()" class="btn btn-default btn-primary" data-toggle="modal" data-target="#outputModal">Guardar</button>
            <button id="btn_cancelar" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>    
        </div>
    </div>
        <script>
        $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        });
        </script>';
        $content -> set_body($body);
        $content -> set_footer();
        $content ->build_content();
        //echo $body;
