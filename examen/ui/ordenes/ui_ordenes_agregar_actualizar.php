<?php
include_once($_SERVER['DOCUMENT_ROOT']."/logic/contenido/logic_contenido.php");
include_once($_SERVER['DOCUMENT_ROOT']."/logic/ordenes/logic_ordenes.php");
include_once($_SERVER['DOCUMENT_ROOT']."/logic/clientes/logic_clientes.php");
include_once($_SERVER['DOCUMENT_ROOT']."/logic/platillos/logic_platillos.php");


$id_orden = "";
$fecha_orden = "";
$hora_orden = "";
$numero_mesa_orden = "";
$id_cliente_orden = "";
$id_platillo_orden = "";
$estado_orden = "";

if(isset($_GET['id_orden'])){
    $id_orden = $_GET['id_orden'];
}
$orden = new logic_ordenes;
$orden_array = $orden ->list_orden_by_id($id_orden);
$clientes = new logic_clientes;
$clientes_array = $clientes ->list_all_clientes();
$platillos = new logic_platillos;
$platillos_array = $platillos ->list_all_platillos_index();
		
$body_clientes = "";
$body_platillos = "";
$options_platillos ="";
$max_row = 0;
//echo $orden_array[0]["id_cliente_orden"];
if (!empty($clientes_array)) {
	foreach($clientes_array as $row_key => $row)
	{
		$body_clientes = $body_clientes.'<option value="'.$row["id_cliente"].'" '.$retVal = ($row["id_cliente"] == $orden_array[0]["id_cliente_orden"] ? "selected" : "").'>'.$row["nombre_cliente"].'</option>';
		//echo $body_clientes;
	}
}
$platillos_array_orden = $orden ->list_all_platillos_ordenes_by_orden($id_orden);
if ($platillos_array_orden) {
	foreach($platillos_array_orden as $row_key_platillo => $row_platillo){
		if (count($platillos_array_orden)>0) {
			$body_platillos = $body_platillos.'<div class="input-group" id="orden_platillo'.$row_key_platillo.'"> 
													<select class="form-control group_platillos">';
													
			if (!empty($platillos_array)) {
				foreach($platillos_array as $row_key => $row)
				{
					//$options_platillos  = $options_platillos.'<option value="'.$row["id_platillo"].'">'.$row["nombre_platillo"].'</option>'; 
					$body_platillos = $body_platillos.'<option value="'.$row["id_platillo"].'" '.$retVal = ($row["id_platillo"] ==$row_platillo["id_platillo"] ? "selected" : "").'>'.$row["nombre_platillo"].'</option>';
					//echo $body_clientes;
				}
			}
			$body_platillos = $body_platillos.'</select>
												<div class="input-group-btn">
														<button onclick="eliminar_platillo_orden_agregar('.$row_key_platillo.')"  class="btn btn-danger" type="submit">
														<i class="glyphicon glyphicon-minus"></i> 
													</button>
												</div>
											</div>';
		}
		$max_row = $row_key_platillo;
	}
}
$options_platillos = $options_platillos.'<div class="input-group" id="orden_platillo_cambiar"> 
<select class="form-control group_platillos no_agregar">';
foreach($platillos_array as $row_key => $row)
				{
					$options_platillos  = $options_platillos.'<option value="'.$row["id_platillo"].'">'.$row["nombre_platillo"].'</option>'; 
					//$body_platillos = $body_platillos.'<option value="'.$row["id_platillo"].'" '.$retVal = ($row["id_platillo"] ==$row_platillo["id_platillo"] ? "selected" : "").'>'.$row["nombre_platillo"].'</option>';
					//echo $body_clientes;
				}
$options_platillos = $options_platillos.'</select>
	<div class="input-group-btn">
			<button onclick="eliminar_platillo_orden_agregar(_cambiar)"  class="btn btn-danger" type="submit">
			<i class="glyphicon glyphicon-minus"></i> 
		</button>
	</div>
</div>';
// echo $options_platillos;
// exit;
// echo $body_platillos;
// exit;

// if (!empty($platillos_array)) {
// 	foreach($platillos_array as $row_key => $row)
// 	{
// 		$body_platillos = $body_platillos.'<option value="'.$row["id_platillo"].'" '.$retVal = ($row["id_platillo"] == $orden_array[0]["id_platillo"] ? "selected" : "").'>'.$row["nombre_cliente"].'</option>';
// 		//echo $body_clientes;
// 	}
// }
//echo $body_clientes;
$body=""; 
$content = new logic_contenido;
//$content -> set_header();
// echo $orden_array[0]["nombre_orden"];
// exit;
if (empty($id_orden)) {    
$body = $body.'
<div>
	<div>
		<div id="opciones_platillos" class="border" style="display: none;">'.$options_platillos.'</div>
		<label class="control-label col-sm-2" for="fecha_orden">Fecha:</label>
		<div>
			<input type="date" class="form-control" id="fecha_orden" name="fecha_orden" placeholder="Digite la fecha de la orden">
			</div>
			<br>
		</div>
    </div>
    <div>
		<label class="control-label col-sm-2" for="hora_orden">Hora:</label>
		<div>
			<input type="time" class="form-control" id="hora_orden" name="hora_orden" placeholder="Digite la Hora de la orden">
			<br>
		</div>
    </div>	
    <div>
		<label class="control-label col-sm-2" for="numero_mesa_orden">Numero de Mesa:</label>
		<div>
			<input type="text" class="form-control" id="numero_mesa_orden" name="numero_mesa_orden" placeholder="Digite el Numero de Mesa de la orden">
			<br>
		</div>
    </div>	

    <div>
		<label class="control-label col-sm-2" for="id_cliente_orden">Cliente:</label>
		<div>
		<select name="id_cliente_orden" id="id_cliente_orden" class="form-control">'.$body_clientes.'</select>
			<br>
		</div>
    </div>	
    	
	<div id="platillo_cont" class="border" style="display: none;">0</div>
		<p>
			<button onclick="agregar_platillo_orden_agregar()" id="btn_agregar_platillo" name="btn_agregar_platillo" type="button" class="btn btn-success">Agregar Platillo</button>
		</p>
		<p><div id="id_platillos"></p>
	</div>

    <div>
		<label class="control-label col-sm-2" for="estado_orden">Estado:</label>
		<div>
		<select name="estado_orden" id="estado_orden" class="form-control">
		<option value="P">Pendiente</option>
		<option value="R">Rechazada por el cliente</option>
		<option value="C">Preparando por la cocina</option>
		<option value="L">Lista</option>
		<option value="E">Entregada</option>
	  </select>
	  			<br>
		</div>
    </div>	

	<div> 
		<div class="col-sm-offset-2 col-sm-10">
		</div>
	</div>
</div>
<!-- Modal -->
<div id="outputModal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Guardar orden</h4>
        </div>
        <div class="modal-body">
            <div id="output"> </div>
        </div>
        <div class="modal-footer">
        <a id="btn_aceptar" type="button" href="/ui/ordenes/ui_ordenes_index.php" target="_self" class="btn btn-sm btn-primary">Aceptar</a>
        </div>
    </div>
    </div>
</div>
';
echo $body;
} else {
	$platillos = "";
	$max_row = "0";
	$platillos_array = $orden ->list_all_platillos_ordenes_by_orden($id_orden);
	//echo count($platillos_array);
	if ($platillos_array) {
		foreach($platillos_array as $row_key_platillo => $row_platillo){
			if (count($platillos_array)>=1) {
				$platillos = $platillos.
				'<div class="input-group" id="orden_platillo'.$row_key_platillo.'"> 
					<input  type="text" class="form-control group_platillos" value="'.$row_platillo["id_platillo"].'">
					<div class="input-group-btn">
						<button onclick="eliminar_platillo_orden_agregar('.$row_key_platillo.')"  class="btn btn-danger" type="submit">
							<i class="glyphicon glyphicon-minus"></i> 
						</button>
					</div>
				</div>';
				$max_row = $row_key_platillo;
			}
		}
	}
    
$body = $body.'
<div>
	<div>
		<div id="opciones_platillos" class="border" style="display: none;">'.$options_platillos.'</div>
		<input type="hidden" name="id_orden" id="id_orden" value="'.$id_orden.'"/>
		<label class="control-label col-sm-2" for="fecha_orden">Fecha:</label>
		<div>
			<input type="date" class="form-control" id="fecha_orden" name="fecha_orden" placeholder="Digite la fecha de la orden" value="'.$orden_array[0]["fecha_orden"].'">
			<br>
		</div>
    </div>
    <div>
		<label class="control-label col-sm-2" for="hora_orden">Hora:</label>
		<div>
			<input type="time" class="form-control" id="hora_orden" name="hora_orden" placeholder="Digite la Hora de la orden" value="'.$orden_array[0]["hora_orden"].'">
			<br>
		</div>
    </div>	
    <div>
		<label class="control-label col-sm-2" for="numero_mesa_orden">Numero de Mesa:</label>
		<div>
			<input type="text" class="form-control" id="numero_mesa_orden" name="numero_mesa_orden" placeholder="Digite el Numero de Mesa de la orden" value="'.$orden_array[0]["numero_mesa_orden"].'">
			<br>
		</div>
    </div>	

    <div>
		<label class="control-label col-sm-2" for="id_cliente_orden">Cliente:</label>
		<div>
		<select name="id_cliente_orden" id="id_cliente_orden" class="form-control">'.$body_clientes.'</select>
			<br>
		</div>
    </div>
    	
	<div id="platillo_cont" class="border" style="display: none;">'.$max_row.'</div>
		<p>
			<button onclick="agregar_platillo_orden_agregar()" id="btn_agregar_platillo" name="btn_agregar_platillo" type="button" class="btn btn-success">Agregar Platillo</button>
		</p>
		<p><div id="id_platillos"></p>
		'.$body_platillos.'
	</div>

    <div>
		<label class="control-label col-sm-2" for="estado_orden">Estado:</label>
		<div>
			<select name="estado_orden" id="estado_orden" class="form-control">
				<option value="P"'.$retVal = ($orden_array[0]["estado_orden"]=="P" ? "selected" : "").'>Pendiente</option>
				<option value="R"'.$retVal = ($orden_array[0]["estado_orden"]=="R" ? "selected" : "").'>Rechazada por el cliente</option>
				<option value="C"'.$retVal = ($orden_array[0]["estado_orden"]=="C" ? "selected" : "").'>Preparando por la cocina</option>
				<option value="L"'.$retVal = ($orden_array[0]["estado_orden"]=="L" ? "selected" : "").'>Lista</option>
				<option value="E"'.$retVal = ($orden_array[0]["estado_orden"]=="E" ? "selected" : "").'>Entregada</option>
			</select>
			<br>
		</div>
    </div>	
	<div> 
		<div class="col-sm-offset-2 col-sm-10">
		</div>
	</div>
</div>
<!-- Modal -->
<div id="outputModal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered  modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Guardar orden</h4>
        </div>
        <div class="modal-body">
            <div id="output"> </div>
        </div>
        <div class="modal-footer">
        <a id="btn_aceptar" type="button" href="/ui/ordenes/ui_ordenes_index.php" target="_self" class="btn btn-sm btn-primary">Aceptar</a>
        </div>
    </div>
    </div>
</div>
';
echo $body;
}

//$content -> set_body($body);
//$content -> set_footer();
//$content ->build_content();

?>

