<?php
include_once($_SERVER['DOCUMENT_ROOT']."/logic/contenido/logic_contenido.php");
include_once($_SERVER['DOCUMENT_ROOT']."/logic/ordenes/logic_ordenes.php");

$id_orden = "";
$fecha_orden = "";
$hora_orden = "";
$numero_mesa_orden = "";
$id_cliente_orden = "";
$platillos_orden = []; 
$estado_orden = "";


if(isset($_POST['id_orden'])){
    $id_orden = $_POST['id_orden'];
}
if (isset($_POST['fecha_orden'])) {
    $fecha_orden = $_POST['fecha_orden'];
}
if (isset($_POST['hora_orden'])){
    $hora_orden = $_POST['hora_orden'];
}
if (isset($_POST['numero_mesa_orden'])) {
    $numero_mesa_orden = $_POST['numero_mesa_orden'];
}
if (isset($_POST['id_cliente_orden'])) {
    $id_cliente_orden = $_POST['id_cliente_orden'];
}

if (isset($_POST['platillos_orden'])) {
    $platillos_orden = $_POST['platillos_orden'];
}

if (isset($_POST['estado_orden'])) {
    $estado_orden = $_POST['estado_orden'];
}

//  echo count($platillos_orden);
//  exit;


$orden = new logic_ordenes;
if(empty($id_orden)){
    // echo "insert";
    // exit;
    $orden_id = $orden ->insert_orden($fecha_orden,$hora_orden,$numero_mesa_orden,$id_cliente_orden,$estado_orden);
    if ($orden_id) {
        $platillos_orden = array_filter($platillos_orden);
        if (count($platillos_orden)>0) {
            foreach ($platillos_orden as $key => $value) {
                //echo $value;                
                if (!$orden ->insert_platillo_orden($orden_id,$value)) {
                    $orden ->delete_orden($orden_id);
                    $_SESSION["message"] = "Error en la Operacion";
                    echo $_SESSION["message"];
                    exit;
                }
                else {
                    $_SESSION["message"] = "Operacion Exitosa";
                }
            }
        }else {
            $_SESSION["message"] = "Operacion Exitosa";
            //echo $_SESSION["message"];
        }
    }else {
        $_SESSION["message"] = "Error en la Operacion 2";
        //echo $_SESSION["message"];

    }
}else{
    // echo "update";
    // exit;
 
    $orden ->update_orden($id_orden,$fecha_orden,$hora_orden,$numero_mesa_orden,$id_cliente_orden,$estado_orden);
    //session_start();
    //echo $_SESSION["message"];
    $platillos_orden = array_filter($platillos_orden);
    // echo count($platillos_orden);
    // exit;
    if (count($platillos_orden)>0) {
        $orden ->delete_all_platillos_ordenes_by_orden($id_orden);
        foreach ($platillos_orden as $key => $value) {
            //echo $value;                
            if (!$orden ->insert_platillo_orden($id_orden,$value)) {
               // $orden ->delete_orden($orden_id);
                $_SESSION["message"] = "Error en la Operacion";
                echo $_SESSION["message"];
                exit;
            }
            else {
                $_SESSION["message"] = "Operacion Exitosa";
            }
        }
    }else {
        $orden ->delete_all_platillos_ordenes_by_orden($id_orden);
        $_SESSION["message"] = "Operacion Exitosa";
        //echo $_SESSION["message"];
    }
}

echo $_SESSION["message"];

?>