<?php
include_once($_SERVER['DOCUMENT_ROOT']."/logic/clientes/logic_clientes.php");
include_once($_SERVER['DOCUMENT_ROOT']."/logic/contenido/logic_contenido.php");

//$index = new ui_list_all_clientes_index();
//$index -> build_index();
$content = new logic_contenido;
$content -> set_header();
//$content -> set_body($_SERVER['DOCUMENT_ROOT']."/ui/clientes/ui_list_all_clientes_index.php");

        $body="";    
        $link ="";
        $clientes = new logic_clientes;
        $clientes_array = $clientes ->list_all_clientes();
        $body = $body.
        '<div class="container mt-3">
        <h2>Clientes</h2>
        <p>Desea Agregar un cliente?
        <button onclick="ver_cliente(\'\',\'A\')" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#checDeletekModal">Agregar</button> 
        
        </p>  
        <hr noshade>
        <p>Digite algun valor relacionado al cliente que desee buscar:</p>  
        <input class="form-control" id="myInput" type="text" placeholder="Buscar..">
        <br>
        <table class="table table-bordered table-condensed table-striped text-nowrap">
            <thead>
            <tr>
                <th>Identificador</th>
                <th>Nombre</th>
                <th>Cédula</th>
                <th>Teléfono</th>
                <th>Correo electrónico</th>
                <th>Dirección exacta</th>
                <th>Provincia</th>
                <th>Cantón</th>
                <th>Distrito</th>
                <th>Hacer</th>
            </tr>
            </thead>
            <tbody id="myTable" class="table-striped">';
            // echo count($clientes_array);
            // exit;
            if (!empty($clientes_array)) {
                foreach($clientes_array as $row_key => $row)
                {
                    $body = $body.'<tr>
                        <td id="id_cliente'.$row["id_cliente"].'">'.$row["id_cliente"].'</td>
                        <td id="nombre_cliente'.$row["id_cliente"].'">'.$row["nombre_cliente"].'</td>
                        <td id="cedula_cliente'.$row["id_cliente"].'">'.$row["cedula_cliente"].'</td>
                        <td id="telefono_cliente'.$row["id_cliente"].'">'.$row["telefono_cliente"].'</td>
                        <td id="email_cliente'.$row["id_cliente"].'">'.$row["email_cliente"].'</td>
                        <td id="direccion_cliente'.$row["id_cliente"].'" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 35ch;">'.$row["direccion_cliente"].' </td>
                        <td id="provincia_cliente'.$row["id_cliente"].'">'.$row["provincia_cliente"].'</td>
                        <td id="canton_cliente'.$row["id_cliente"].'">'.$row["canton_cliente"].'</td>
                        <td id="distrito_cliente'.$row["id_cliente"].'">'.$row["distrito_cliente"].'</td>
                        <td>
                            <button onclick="ver_cliente('.$row["id_cliente"].',\'C\')" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#checDeletekModal">Ver</button> 
                            <button onclick="ver_cliente('.$row["id_cliente"].',\'U\')" type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#checDeletekModal">Editar</button> 
                            <button onclick="ver_cliente('.$row["id_cliente"].',\'D\')" type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#checDeletekModal">Eliminar</button>                           
                        </td>
                    </tr>';
                    //$body = $body.$row["nombre_cliente"];// nombre de la columna
                }
            }
        $body = $body.'</tbody>
            <tfoot>
                <tr>
                    <th>Identificador</th>
                    <th>Nombre</th>
                    <th>Cédula</th>
                    <th>Teléfono</th>
                    <th>Correo electrónico</th>
                    <th>Dirección exacta</th>
                    <th>Provincia</th>
                    <th>Cantón</th>
                    <th>Distrito</th>
                    <th>Hacer</th>
                </tr>
            </tfoot>
        </table>            
        </div>
        <!-- Modal -->
    <div id="checDeletekModal" class="modal fade" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered">    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal_title" class="modal-title">Ver cliente</h4>
            </div>
            <div id="modal_body" class="modal-body">
                    <label>Consecutivo:</label>
                    <div>
                        <div id="id_factura_output">1</div>
                    </div>
                    <hr noshade>
                    <label>Fecha:</label>
                    <div>
                        <div id="fecha_factura_output">nombre Producto1</div>
                    </div>
                    <hr>
                    <label>Hora:</label>
                    <div>
                        <div id="hora_factura_output">111</div>
                    </div>
                    <hr>
                    <label>Numero de orden:</label>
                    <div>
                        <div id="id_orden_factura_output">111</div>
                    </div>
                    <hr>
                    <label>Subtotal:</label>
                    <div>
                        <div id="subtotal_factura_output">111</div>
                    </div>
                    <hr>
                    <label>Impuesto de Servicio:</label>
                    <div>
                        <div id="impuesto_servicio_factura_output">presentacion producto 1</div>
                    </div>
                    <hr>
                    <label>IVA:</label>
                    <div>
                        <div id="iva_factura_output">presentacion producto 1</div>
                    </div>
                    <hr>
                    <label>Total:</label>
                    <div>
                        <div id="total_factura_output">presentacion producto 1</div>
                    </div>
            </div>
            <div id="modal_message" class="modal-body"></div>

            <div class="modal-footer">
            <button onclick="eliminar_cliente()" id="btn_aceptar" type="button" class="btn btn-danger">Aceptar</button>
            <button onclick="eliminar_cliente_refrescar()" id="btn_aceptar_cerrar" type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
            <button id="btn_guardar" onclick="guardar_cliente()" class="btn btn-default btn-primary" data-toggle="modal" data-target="#outputModal">Guardar</button>
            <button id="btn_cancelar" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>    
        </div>
    </div>
        <script>
        $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        });
        </script>';
        $content -> set_body($body);
        $content -> set_footer();
        $content ->build_content();
        //echo $body;
