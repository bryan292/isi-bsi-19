<?php
include_once($_SERVER['DOCUMENT_ROOT']."/logic/contenido/logic_contenido.php");
include_once($_SERVER['DOCUMENT_ROOT']."/logic/clientes/logic_clientes.php");

$id_cliente = "";
$nombre_cliente = "";
$cedula_cliente = "";
$telefono_cliente = "";
$email_cliente = "";
$direccion_cliente = "";
$provincia_cliente = "";
$canton_cliente = "";
$distrito_cliente = "";

if(isset($_GET['id_cliente'])){
    $id_cliente = $_GET['id_cliente'];
}
$cliente = new logic_clientes;
$cliente_array = $cliente ->list_cliente_by_id($id_cliente);

$body=""; 
$content = new logic_contenido;
//$content -> set_header();
// echo $cliente_array[0]["nombre_cliente"];
// exit;
if (empty($id_cliente)) {    
$body = $body.'
<div>
    <div>
		<label class="control-label col-sm-2" for="nombre_cliente">Nombre:</label>
		<div>
			<input type="text" class="form-control" id="nombre_cliente" name="nombre_cliente" placeholder="Digite el nombre del Cliente">
			<br>
		</div>
    </div>
    <div>
		<label class="control-label col-sm-2" for="cedula_cliente">Cédula:</label>
		<div>
			<input type="text" class="form-control" id="cedula_cliente" name="cedula_cliente" placeholder="Digite la Cédula del Cliente">
			<br>
		</div>
    </div>	
    <div>
		<label class="control-label col-sm-2" for="telefono_cliente">Teléfono:</label>
		<div>
			<input type="text" class="form-control" id="telefono_cliente" name="telefono_cliente" placeholder="Digite el Teléfono del Cliente">
			<br>
		</div>
    </div>	

    <div>
		<label class="control-label col-sm-2" for="email_cliente">Correo electrónico:</label>
		<div>
			<input type="text" class="form-control" id="email_cliente" name="email_cliente" placeholder="Digite el Correo del Cliente">
			<br>
		</div>
    </div>	
    
    <div>
		<label class="control-label col-sm-2" for="direccion_cliente">Dirección exacta:</label>
		<div> 
			<textarea rows="4" cols="50" class="form-control" id="direccion_cliente" style ="resize: none;" name = "direccion_cliente" placeholder="Digite la Direccion del cliente"></textarea>
			<br>
		</div>
    </div>
	
    <div>
		<label class="control-label col-sm-2" for="provincia_cliente">Provincia:</label>
		<div>
			<input type="text" class="form-control" id="provincia_cliente" name="provincia_cliente" placeholder="Digite la Provincia del Cliente">
			<br>
		</div>
    </div>	

    <div>
		<label class="control-label col-sm-2" for="canton_cliente">Cantón:</label>
		<div>
			<input type="text" class="form-control" id="canton_cliente" name="canton_cliente" placeholder="Digite el Cantón del Cliente">
			<br>
		</div>
    </div>	
	
    <div>
		<label class="control-label col-sm-2" for="distrito_cliente">Distrito:</label>
		<div>
			<input type="text" class="form-control" id="distrito_cliente" name="distrito_cliente" placeholder="Digite el Distrito del Cliente">
			<br>
		</div>
    </div>	
    

	<div> 
		<div class="col-sm-offset-2 col-sm-10">
		</div>
	</div>
</div>
<!-- Modal -->
<div id="outputModal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Guardar cliente</h4>
        </div>
        <div class="modal-body">
            <div id="output"> </div>
        </div>
        <div class="modal-footer">
        <a id="btn_aceptar" type="button" href="/ui/clientes/ui_clientes_index.php" target="_self" class="btn btn-sm btn-primary">Aceptar</a>
        </div>
    </div>
    </div>
</div>
';
echo $body;
} else {
    
$body = $body.'
<div>
    <div>
		<input type="hidden" name="id_cliente" id="id_cliente" value="'.$id_cliente.'"/>
		<label class="control-label col-sm-2" for="nombre_cliente">Nombre:</label>
		<div>
			<input type="text" class="form-control" id="nombre_cliente" name="nombre_cliente" placeholder="Digite el nombre del Cliente" value="'.$cliente_array[0]["nombre_cliente"].'">
			<br>
		</div>
    </div>
    <div>
		<label class="control-label col-sm-2" for="cedula_cliente">Cédula:</label>
		<div>
			<input type="text" class="form-control" id="cedula_cliente" name="cedula_cliente" placeholder="Digite la Cédula del Cliente" value="'.$cliente_array[0]["cedula_cliente"].'">
			<br>
		</div>
    </div>	
    <div>
		<label class="control-label col-sm-2" for="telefono_cliente">Teléfono:</label>
		<div>
			<input type="text" class="form-control" id="telefono_cliente" name="telefono_cliente" placeholder="Digite el Teléfono del Cliente" value="'.$cliente_array[0]["telefono_cliente"].'">
			<br>
		</div>
    </div>	

    <div>
		<label class="control-label col-sm-2" for="email_cliente">Correo electrónico:</label>
		<div>
			<input type="text" class="form-control" id="email_cliente" name="email_cliente" placeholder="Digite el Correo del Cliente" value="'.$cliente_array[0]["email_cliente"].'">
			<br>
		</div>
    </div>	
    
    <div>
		<label class="control-label col-sm-2" for="direccion_cliente">Dirección exacta:</label>
		<div> 
			<textarea rows="4" cols="50" class="form-control" id="direccion_cliente" style ="resize: none;" name = "direccion_cliente" placeholder="Digite la Direccion del cliente">'.$cliente_array[0]["direccion_cliente"].'</textarea>
			<br>
		</div>
    </div>
	
    <div>
		<label class="control-label col-sm-2" for="provincia_cliente">Provincia:</label>
		<div>
			<input type="text" class="form-control" id="provincia_cliente" name="provincia_cliente" placeholder="Digite la Provincia del Cliente" value="'.$cliente_array[0]["provincia_cliente"].'">
			<br>
		</div>
    </div>	

    <div>
		<label class="control-label col-sm-2" for="canton_cliente">Cantón:</label>
		<div>
			<input type="text" class="form-control" id="canton_cliente" name="canton_cliente" placeholder="Digite el Cantón del Cliente" value="'.$cliente_array[0]["canton_cliente"].'">
			<br>
		</div>
    </div>	
	
    <div>
		<label class="control-label col-sm-2" for="distrito_cliente">Distrito:</label>
		<div>
			<input type="text" class="form-control" id="distrito_cliente" name="distrito_cliente" placeholder="Digite el Distrito del Cliente" value="'.$cliente_array[0]["distrito_cliente"].'">
			<br>
		</div>
    </div>	
    

	<div> 
		<div class="col-sm-offset-2 col-sm-10">
		</div>
	</div>
</div>
<!-- Modal -->
<div id="outputModal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered  modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Guardar Cliente</h4>
        </div>
        <div class="modal-body">
            <div id="output"> </div>
        </div>
        <div class="modal-footer">
        <a id="btn_aceptar" type="button" href="/ui/clientes/ui_clientes_index.php" target="_self" class="btn btn-sm btn-primary">Aceptar</a>
        </div>
    </div>
    </div>
</div>
';
echo $body;
}

//$content -> set_body($body);
//$content -> set_footer();
//$content ->build_content();

?>

