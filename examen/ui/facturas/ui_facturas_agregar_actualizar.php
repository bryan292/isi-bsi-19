<?php
include_once($_SERVER['DOCUMENT_ROOT']."/logic/contenido/logic_contenido.php");
include_once($_SERVER['DOCUMENT_ROOT']."/logic/facturas/logic_facturas.php");

$id_factura = "";
$fecha_factura = "";
$hora_factura = "";
$id_orden_factura = "";
$subtotal_factura = "";
$impuesto_servicio_factura = "";
$iva_factura = "";
$total_factura = "";

if(isset($_GET['id_factura'])){
    $id_factura = $_GET['id_factura'];
}
$factura = new logic_facturas;
$factura_array = $factura ->list_factura_by_id($id_factura);

$body=""; 
$content = new logic_contenido;
//$content -> set_header();
// echo $factura_array[0]["nombre_factura"];
// exit;
if (empty($id_factura)) {    
$body = $body.'
<div>
    <div>
		<label class="control-label col-sm-2" for="fecha_factura">Fecha:</label>
		<div>
			<input type="text" class="form-control" id="fecha_factura" name="fecha_factura" placeholder="Digite la fecha de la factura">
			<br>
		</div>
    </div>
    <div>
		<label class="control-label col-sm-2" for="hora_factura">Hora:</label>
		<div>
			<input type="text" class="form-control" id="hora_factura" name="hora_factura" placeholder="Digite la Hora de la factura">
			<br>
		</div>
    </div>	
    <div>
		<label class="control-label col-sm-2" for="id_orden_factura">Numero de orden:</label>
		<div>
			<input type="text" class="form-control" id="id_orden_factura" name="id_orden_factura" placeholder="Digite el Id de la Orden de factura">
			<br>
		</div>
    </div>	

    <div>
		<label class="control-label col-sm-2" for="subtotal_factura">Subtotal:</label>
		<div>
			<input type="text" class="form-control" id="subtotal_factura" name="subtotal_factura" placeholder="Digite el Subtotal de la factura">
			<br>
		</div>
    </div>	
	
    <div>
		<label class="control-label col-sm-2" for="impuesto_servicio_factura">Impuesto de Servicio:</label>
		<div>
			<input type="text" class="form-control" id="impuesto_servicio_factura" name="impuesto_servicio_factura" placeholder="Digite el Impuesto de Servicio de la factura">
			<br>
		</div>
    </div>	

    <div>
		<label class="control-label col-sm-2" for="iva_factura">IVA:</label>
		<div>
			<input type="text" class="form-control" id="iva_factura" name="iva_factura" placeholder="Digite el IVA de la factura">
			<br>
		</div>
    </div>	
	
    <div>
		<label class="control-label col-sm-2" for="total_factura">Total:</label>
		<div>
			<input type="text" class="form-control" id="total_factura" name="total_factura" placeholder="Digite el Total de la factura">
			<br>
		</div>
    </div>	
    

	<div> 
		<div class="col-sm-offset-2 col-sm-10">
		</div>
	</div>
</div>
<!-- Modal -->
<div id="outputModal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Guardar factura</h4>
        </div>
        <div class="modal-body">
            <div id="output"> </div>
        </div>
        <div class="modal-footer">
        <a id="btn_aceptar" type="button" href="/ui/facturas/ui_facturas_index.php" target="_self" class="btn btn-sm btn-primary">Aceptar</a>
        </div>
    </div>
    </div>
</div>
';
echo $body;
} else {
    
$body = $body.'
<div>
    <div>
		<input type="hidden" name="id_factura" id="id_factura" value="'.$id_factura.'"/>
		<label class="control-label col-sm-2" for="fecha_factura">Fecha:</label>
		<div>
			<input type="text" class="form-control" id="fecha_factura" name="fecha_factura" placeholder="Digite la Fecha del factura" value="'.$factura_array[0]["fecha_factura"].'">
			<br>
		</div>
    </div>
    <div>
		<label class="control-label col-sm-2" for="hora_factura">Hora:</label>
		<div>
			<input type="text" class="form-control" id="hora_factura" name="hora_factura" placeholder="Digite la Hora de la factura" value="'.$factura_array[0]["hora_factura"].'">
			<br>
		</div>
    </div>	
    <div>
		<label class="control-label col-sm-2" for="id_orden_factura">Numero de orden:</label>
		<div>
			<input type="text" class="form-control" id="id_orden_factura" name="id_orden_factura" placeholder="Digite el Numero de orden de la  factura" value="'.$factura_array[0]["id_orden_factura"].'">
			<br>
		</div>
    </div>	

    <div>
		<label class="control-label col-sm-2" for="subtotal_factura">Subtotal:</label>
		<div>
			<input type="text" class="form-control" id="subtotal_factura" name="subtotal_factura" placeholder="Digite el Subtotal de la factura" value="'.$factura_array[0]["subtotal_factura"].'">
			<br>
		</div>
    </div>	
    
	
    <div>
		<label class="control-label col-sm-2" for="impuesto_servicio_factura">Impuesto de Servicio:</label>
		<div>
			<input type="text" class="form-control" id="impuesto_servicio_factura" name="impuesto_servicio_factura" placeholder="Digite el Impuesto de Servicio de la factura" value="'.$factura_array[0]["impuesto_servicio_factura"].'">
			<br>
		</div>
    </div>	

    <div>
		<label class="control-label col-sm-2" for="iva_factura">IVA:</label>
		<div>
			<input type="text" class="form-control" id="iva_factura" name="iva_factura" placeholder="Digite el IVA de la factura" value="'.$factura_array[0]["iva_factura"].'">
			<br>
		</div>
    </div>	
	
    <div>
		<label class="control-label col-sm-2" for="total_factura">Total:</label>
		<div>
			<input type="text" class="form-control" id="total_factura" name="total_factura" placeholder="Digite el total de la factura" value="'.$factura_array[0]["total_factura"].'">
			<br>
		</div>
    </div>	
    

	<div> 
		<div class="col-sm-offset-2 col-sm-10">
		</div>
	</div>
</div>
<!-- Modal -->
<div id="outputModal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered  modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Guardar factura</h4>
        </div>
        <div class="modal-body">
            <div id="output"> </div>
        </div>
        <div class="modal-footer">
        <a id="btn_aceptar" type="button" href="/ui/facturas/ui_facturas_index.php" target="_self" class="btn btn-sm btn-primary">Aceptar</a>
        </div>
    </div>
    </div>
</div>
';
echo $body;
}

//$content -> set_body($body);
//$content -> set_footer();
//$content ->build_content();

?>

