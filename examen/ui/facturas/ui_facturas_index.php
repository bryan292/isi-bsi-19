<?php
include_once($_SERVER['DOCUMENT_ROOT']."/logic/facturas/logic_facturas.php");
include_once($_SERVER['DOCUMENT_ROOT']."/logic/contenido/logic_contenido.php");

//$index = new ui_list_all_facturas_index();
//$index -> build_index();
$content = new logic_contenido;
$content -> set_header();
//$content -> set_body($_SERVER['DOCUMENT_ROOT']."/ui/facturas/ui_list_all_facturas_index.php");

        $body="";    
        $link ="";
        $facturas = new logic_facturas;
        $facturas_array = $facturas ->list_all_facturas();
        $body = $body.
        '<div class="container mt-3">
        <h2>Facturas</h2>
        <p>Desea Agregar un Factura?
        <button onclick="ver_factura(\'\',\'A\')" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#checDeletekModal">Agregar</button> 
        
        </p>  
        <hr noshade>
        <p>Digite algun valor relacionado al factura que desee buscar:</p>  
        <input class="form-control" id="myInput" type="text" placeholder="Buscar..">
        <br>
        <table class="table table-bordered table-condensed table-striped text-nowrap">
            <thead>
            <tr>
                <th>Identificador</th>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Numero de Orden</th>
                <th>Subtotal</th>
                <th>Impuesto de Servicio</th>
                <th>IVA</th>
                <th>Total</th>
                <th>Hacer</th>
            </tr>
            </thead>
            <tbody id="myTable">';
            // echo count($facturas_array);
            // exit;
            if (!empty($facturas_array)) {
                foreach($facturas_array as $row_key => $row)
                {
                    $body = $body.'<tr>
                    <td id="id_factura'.$row["id_factura"].'">'.$row["id_factura"].'</td>
                    <td id="fecha_factura'.$row["id_factura"].'">'.$row["fecha_factura"].'</td>
                    <td id="hora_factura'.$row["id_factura"].'">'.$row["hora_factura"].'</td>
                    <td id="id_orden_factura'.$row["id_factura"].'">'.$row["id_orden_factura"].'</td>
                    <td id="subtotal_factura'.$row["id_factura"].'">'.$row["subtotal_factura"].'</td>
                    <td id="impuesto_servicio_factura'.$row["id_factura"].'">'.$row["impuesto_servicio_factura"].'</td>
                    <td id="iva_factura'.$row["id_factura"].'">'.$row["iva_factura"].'</td>
                    <td id="total_factura'.$row["id_factura"].'">'.$row["total_factura"].'</td>
                        <td>
                            <button onclick="ver_factura('.$row["id_factura"].',\'C\')" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#checDeletekModal">Ver</button> 
                            <button onclick="ver_factura('.$row["id_factura"].',\'U\')" type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#checDeletekModal">Editar</button> 
                            <button onclick="ver_factura('.$row["id_factura"].',\'D\')" type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#checDeletekModal">Eliminar</button>                           
                        </td>
                    </tr>';
                    //$body = $body.$row["nombre_factura"];// nombre de la columna
                }
            }
        $body = $body.'</tbody>
            <tfoot>
                <tr>
                    <th>Identificador</th>
                    <th>Fecha</th>
                    <th>Hora</th>
                    <th>Numero de Orden</th>
                    <th>Subtotal</th>
                    <th>Impuesto de Servicio</th>
                    <th>IVA</th>
                    <th>Total</th>
                    <th>Hacer</th>
                </tr>
            </tfoot>
        </table>            
        </div>
        <!-- Modal -->
    <div id="checDeletekModal" class="modal fade" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered">    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal_title" class="modal-title">Ver factura</h4>
            </div>
            <div id="modal_body" class="modal-body">
                    <label>Identificador:</label>
                        <div id="id_factura_output">1</div>
                        <hr noshade>
                    <label>Fecha:</label>
                    <div>
                        <div id="fecha_factura_output">nombre Producto1</div>
                    </div>
                    <hr>
                    <label>Hora:</label>
                    <div>
                        <div id="hora_factura_output">111</div>
                    </div>
                    <hr>
                    <label>Numero de orden:</label>
                    <div>
                        <div id="id_orden_factura_output">111</div>
                    </div>
                    <hr>
                    <label>Subtotal:</label>
                    <div>
                        <div id="subtotal_factura_output">111</div>
                    </div>
                    <hr>
                    <label>Impuesto de Servicio:</label>
                    <div>
                        <div id="impuesto_servicio_factura_output">111</div>
                    </div>
                    <hr>
                    <label>IVA:</label>
                    <div>
                        <div id="iva_factura_output">111</div>
                    </div>
                    <hr>
                    <label>Total:</label>
                    <div>
                        <div id="total_factura_output">presentacion producto 1</div>
                    </div>
            </div>
            <div id="modal_message" class="modal-body"></div>

            <div class="modal-footer">
            <button onclick="eliminar_factura()" id="btn_aceptar" type="button" class="btn btn-danger">Aceptar</button>
            <button onclick="eliminar_factura_refrescar()" id="btn_aceptar_cerrar" type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
            <button id="btn_guardar" onclick="guardar_factura()" class="btn btn-default btn-primary" data-toggle="modal" data-target="#outputModal">Guardar</button>
            <button id="btn_cancelar" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>    
        </div>
    </div>
        <script>
        $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        });
        </script>';
        $content -> set_body($body);
        $content -> set_footer();
        $content ->build_content();
        //echo $body;
