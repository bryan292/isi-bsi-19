<?php
include_once($_SERVER['DOCUMENT_ROOT']."/logic/contenido/logic_contenido.php");
include_once($_SERVER['DOCUMENT_ROOT']."/logic/platillos/logic_platillos.php");

$id_platillo = "";
$nombre_platillo = "";
$precio_platillo = "";
$descripcion_platillo = "";
$presentacion_platillo = "";
if(isset($_GET['id_platillo'])){
    $id_platillo = $_GET['id_platillo'];
}
$platillo = new logic_platillos;
$platillo_array = $platillo ->list_platillo_by_id($id_platillo);

$body=""; 
$content = new logic_contenido;
//$content -> set_header();
// echo $platillo_array[0]["nombre_platillo"];
// exit;
if (empty($id_platillo)) {    
$body = $body.'
<div>
    <div>
    <label class="control-label col-sm-2" for="nombre_platillo">Nombre:</label>
    <div>
        <input type="text" class="form-control" id="nombre_platillo" name="nombre_platillo" placeholder="Digite el nombre del Platillo">
        <br>
    </div>
    </div>
    <div>
    <label class="control-label col-sm-2" for="precio_platillo">Precio:</label>
    <div> 
        <input type="text" class="form-control" id="precio_platillo" name="precio_platillo" placeholder="Digite el precio del Platillo">
        <br>
    </div>
    </div>
    
    <div>
    <label class="control-label col-sm-2" for="descripcion_platillo">Descripción:</label>
    <div> 
        <textarea rows="4" cols="50" class="form-control" id="descripcion_platillo" style ="resize: none;"name="descripcion_platillo" placeholder="Digite una descripcion para el Platillo"></textarea>
        <br>
    </div>
    </div>
    <div>
        <label class="control-label col-sm-2" for="presentacion_platillo">Presentación:</label>
        <div> 
            <input type="text" class="form-control" id="presentacion_platillo" name="presentacion_platillo" placeholder="Digite la presentacion del Platillo">
            <br>
        </div>
        </div>
    <div> 
    <div class="col-sm-offset-2 col-sm-10">
    </div>
    </div>
</div>
<!-- Modal -->
<div id="outputModal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Guardar Platillo</h4>
        </div>
        <div class="modal-body">
            <div id="output"> </div>
        </div>
        <div class="modal-footer">
        <a id="btn_aceptar" type="button" href="/ui/platillos/ui_platillos_index.php" target="_self" class="btn btn-sm btn-primary">Aceptar</a>
        </div>
    </div>
    </div>
</div>
';
echo $body;
} else {
    
$body = $body.'
<div>
    <div>
    <input type="hidden" name="id_platillo" id="id_platillo" value="'.$id_platillo.'"/>
    <label class="control-label col-sm-2" for="nombre_platillo">Nombre:</label>
    <div>
        <input type="text" class="form-control" id="nombre_platillo" name="nombre_platillo" placeholder="Digite el nombre del Platillo" value="'.$platillo_array[0]["nombre_platillo"].'">
        <br>
    </div>
    </div>
    <div>
    <label class="control-label col-sm-2" for="precio_platillo">Precio:</label>
    <div> 
        <input type="text" class="form-control" id="precio_platillo" name="precio_platillo" placeholder="Digite el precio del Platillo"  value="'.$platillo_array[0]["precio_platillo"].'">
        <br>
    </div>
    </div>
    
    <div>
    <label class="control-label col-sm-2" for="descripcion_platillo">Descripción:</label>
    <div> 
        <textarea rows="4" cols="50" class="form-control" id="descripcion_platillo" style ="resize: none;"name="descripcion_platillo" placeholder="Digite una descripcion para el Platillo">'.$platillo_array[0]["descripcion_platillo"].'</textarea>
        <br>
    </div>
    </div>
    <div>
        <label class="control-label col-sm-2" for="presentacion_platillo">Presentación:</label>
        <div> 
            <input type="text" class="form-control" id="presentacion_platillo" name="presentacion_platillo" placeholder="Digite la presentacion del Platillo" value="'.$platillo_array[0]["presentacion_platillo"].'">
            <br>
        </div>
        </div>
    <div> 
    <div class="col-sm-offset-2 col-sm-10">
    </div>
    </div>
</div>
<!-- Modal -->
<div id="outputModal" class="modal fade " role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-dialog-centered  modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Guardar Platillo</h4>
        </div>
        <div class="modal-body">
            <div id="output"> </div>
        </div>
        <div class="modal-footer">
        <a id="btn_aceptar" type="button" href="/ui/platillos/ui_platillos_index.php" target="_self" class="btn btn-sm btn-primary">Aceptar</a>
        </div>
    </div>
    </div>
</div>
';
echo $body;
}

//$content -> set_body($body);
//$content -> set_footer();
//$content ->build_content();

?>

