<?php
include_once($_SERVER['DOCUMENT_ROOT']."/logic/platillos/logic_platillos.php");
include_once($_SERVER['DOCUMENT_ROOT']."/logic/contenido/logic_contenido.php");

//$index = new ui_list_all_platillos_index();
//$index -> build_index();
$content = new logic_contenido;
$content -> set_header();
//$content -> set_body($_SERVER['DOCUMENT_ROOT']."/ui/platillos/ui_list_all_platillos_index.php");

        $body="";    
        $link ="";
        $platillos = new logic_platillos;
        $platillos_array = $platillos ->list_all_platillos_index();
        $body = $body.
        '<div class="container mt-3">
        <h2>Platillos</h2>
        <p>Desea Agregar un Platillo?
        <button onclick="ver_platillo(\'\',\'A\')" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#checDeletekModal">Agregar</button> 
        
        </p>  
        <hr noshade>
        <p>Digite algun valor relacionado al platillo que desee buscar:</p>  
        <input class="form-control" id="myInput" type="text" placeholder="Buscar..">
        <br>
        <table class="table table-bordered table-condensed table-striped text-nowrap">
            <thead>
            <tr>
                <th>Identificador</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Descripción</th>
                <th>Presentación</th>
                <th>Hacer</th>
            </tr>
            </thead>
            <tbody id="myTable">';
            // echo count($platillos_array);
            // exit;
            if (!empty($platillos_array)) {
                foreach($platillos_array as $row_key => $row)
                {
                    $body = $body.'<tr>
                        <td id="id_platillo'.$row["id_platillo"].'">'.$row["id_platillo"].'</td>
                        <td id="nombre_platillo'.$row["id_platillo"].'">'.$row["nombre_platillo"].'</td>
                        <td id="precio_platillo'.$row["id_platillo"].'">'.$row["precio_platillo"].'</td>
                        <td class="text-nowrap" id="descripcion_platillo'.$row["id_platillo"].'"style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 35ch;">'.$row["descripcion_platillo"].'</td>
                        <td id="presentacion_platillo'.$row["id_platillo"].'">'.$row["presentacion_platillo"].'</td>
                        <td>
                            <button onclick="ver_platillo('.$row["id_platillo"].',\'C\')" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#checDeletekModal">Ver</button> 
                            <button onclick="ver_platillo('.$row["id_platillo"].',\'U\')" type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#checDeletekModal">Editar</button> 
                            <button onclick="ver_platillo('.$row["id_platillo"].',\'D\')" type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#checDeletekModal">Eliminar</button>                           
                        </td>
                    </tr>';
                    //$body = $body.$row["nombre_platillo"];// nombre de la columna
                }
            }
        $body = $body.'</tbody>
            <tfoot>
                <tr>
                    <th>Identificador</th>
                    <th>Nombre</th>
                    <th>Precio</th>
                    <th>Descripción</th>
                    <th>Presentación</th>
                    <th>Hacer</th>
                </tr>
            </tfoot>
        </table>            
        </div>
        <!-- Modal -->
    <div id="checDeletekModal" class="modal fade" role="dialog" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered">    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal_title" class="modal-title">Ver Platillo</h4>
            </div>
            <div id="modal_body" class="modal-body">
                    <label>Identificador:</label>
                        <div id="id_platillo_output">1</div>
                        <hr noshade>
                    <label>Nombre:</label>
                    <div>
                        <div id="nombre_platillo_output">nombre Producto1</div>
                    </div>
                    <hr>
                    <label>Precio:</label>
                    <div>
                        <div id="precio_platillo_output">111</div>
                    </div>
                    <hr>
                    <label>Descripción:</label>
                    <div >
                        <div id="descripcion_platillo_output" style="word-wrap: break-word;" >descripcion producto 1</div>
                    </div>
                    <hr>
                    <label>Presentación:</label>
                    <div>
                        <div id="presentacion_platillo_output">presentacion producto 1</div>
                    </div>
            </div>
            <div id="modal_message" class="modal-body"></div>

            <div class="modal-footer">
            <button onclick="eliminar_platillo()" id="btn_aceptar" type="button" class="btn btn-danger">Aceptar</button>
            <button onclick="eliminar_platillo_refrescar()" id="btn_aceptar_cerrar" type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
            <button id="btn_guardar" onclick="guardar_platillo()" class="btn btn-default btn-primary" data-toggle="modal" data-target="#outputModal">Guardar</button>
            <button id="btn_cancelar" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>    
        </div>
    </div>
        <script>
        $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
        });
        </script>';
        $content -> set_body($body);
        $content -> set_footer();
        $content ->build_content();
        //echo $body;
