<?php
include_once($_SERVER['DOCUMENT_ROOT']."/logic/contenido/logic_contenido.php");
include_once($_SERVER['DOCUMENT_ROOT']."/logic/platillos/logic_platillos.php");

$id_platillo = "";
$nombre_platillo = "";
$precio_platillo = "";
$descripcion_platillo = "";
$presentacion_platillo = "";
if(isset($_POST['id_platillo'])){
    $id_platillo = $_POST['id_platillo'];
}
if (isset($_POST['nombre_platillo'])) {
    $nombre_platillo = $_POST['nombre_platillo'];
}
if (isset($_POST['precio_platillo'])){
    $precio_platillo = $_POST['precio_platillo'];
}
if (isset($_POST['descripcion_platillo'])) {
    $descripcion_platillo = $_POST['descripcion_platillo'];
}
if (isset($_POST['presentacion_platillo'])) {
    $presentacion_platillo = $_POST['presentacion_platillo'];
}

$platillo = new logic_platillos;
if(empty($id_platillo)){
    // echo "insert";
    // exit;
    echo $platillo ->insert_platillo($nombre_platillo,$precio_platillo,$descripcion_platillo,$presentacion_platillo);
}else{
    // echo "update";
    // exit;
    echo $platillo ->update_platillo($id_platillo,$nombre_platillo,$precio_platillo,$descripcion_platillo,$presentacion_platillo);
}
?>

