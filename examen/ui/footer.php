<script src="/js/datepicker.js"></script>
  <script>
    $(function() {
      $('[data-toggle="datepicker"]').datepicker({
        autoHide: true,
        zIndex: 2048,
        dateFormat: 'dd-mm-yy'
      });
    });
    </script>
</body>
</html>